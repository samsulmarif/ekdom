-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 14 Sep 2021 pada 13.22
-- Versi server: 8.0.26-0ubuntu0.20.04.2
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekdom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(234, '2014_10_12_000000_create_users_table', 1),
(235, '2014_10_12_100000_create_password_resets_table', 1),
(236, '2019_08_19_000000_create_failed_jobs_table', 1),
(237, '2021_09_08_072350_create_tbl_pertanyaan', 1),
(238, '2021_09_08_234029_create_tbl_evaluasi', 1),
(239, '2021_09_09_114206_create_tbl_hasil_evaluasi', 1),
(240, '2021_09_09_120334_create_tbl_hasil_evaluasi_2', 1),
(241, '2021_09_12_074033_create_tbl_mahaiswa', 1),
(242, '2021_09_12_074045_create_tbl_dosen', 1),
(243, '2021_09_12_231311_create_tbl_evaluasi_prasarana', 1),
(244, '2021_09_12_231327_create_tbl_pertanyaan_prasarana', 1),
(245, '2021_09_12_231724_create_tbl_hasil_evaluasi_prasarana', 1),
(246, '2021_09_12_231731_create_tbl_hasil_evaluasi_prasarana_2', 1),
(247, '2021_09_13_121610_create_tbl_pertanyaan_staf', 1),
(248, '2021_09_13_121742_create_tbl_evaluasi_staf', 1),
(249, '2021_09_13_121801_create_tbl_hasil_evaluasi_staf', 1),
(250, '2021_09_13_121832_create_tbl_hasil_evaluasi_staf_2', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_dosen`
--

CREATE TABLE `tbl_dosen` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_dosen`
--

INSERT INTO `tbl_dosen` (`id`, `name`, `noreg`, `role`, `created_at`, `updated_at`) VALUES
(1, 'dosenekdom', 'EKDOM-700838', 'dosen', '2021-09-13 17:57:21', '2021-09-13 17:57:21'),
(2, 'Dr.eng.laagusu.S.Si.,M.Si', 'EKDOM-881269', 'dosen', '2021-09-13 19:52:37', '2021-09-13 19:52:37'),
(3, 'Lina Lestari', 'EKDOM-303794', 'dosen', '2021-09-13 20:05:05', '2021-09-13 20:05:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_evaluasi`
--

CREATE TABLE `tbl_evaluasi` (
  `id` bigint UNSIGNED NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_evaluasi`
--

INSERT INTO `tbl_evaluasi` (`id`, `noreg`, `evaluasi`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 'EKDOM-209421', 'evaluasi kinerja dosen dan prasarana', 5, '2021-09-13 17:42:39', '2021-09-13 17:42:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_evaluasi_prasarana`
--

CREATE TABLE `tbl_evaluasi_prasarana` (
  `id` bigint UNSIGNED NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_evaluasi_prasarana`
--

INSERT INTO `tbl_evaluasi_prasarana` (`id`, `noreg`, `evaluasi`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 'EKDOM-209421', 'evaluasi prasarana', 5, '2021-09-13 17:44:41', '2021-09-13 17:44:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_evaluasi_staf`
--

CREATE TABLE `tbl_evaluasi_staf` (
  `id` bigint UNSIGNED NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nilai` bigint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_evaluasi_staf`
--

INSERT INTO `tbl_evaluasi_staf` (`id`, `noreg`, `evaluasi`, `nilai`, `created_at`, `updated_at`) VALUES
(1, 'EKDOM-209421', 'evaluasi staf', 5, '2021-09-13 18:05:57', '2021-09-13 19:23:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_evaluasi`
--

CREATE TABLE `tbl_hasil_evaluasi` (
  `id` bigint UNSIGNED NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_hasil_evaluasi`
--

INSERT INTO `tbl_hasil_evaluasi` (`id`, `noreg`, `kategori`, `nim`, `jurusan`, `saran`, `created_at`, `updated_at`) VALUES
(1, 'EKDOM-308112', 'evaluasi kinerja dosen dan prasarana', 'F1B118038', 'fisika fakultas MIPA', 'kuliah online', '2021-09-13 17:50:40', '2021-09-13 17:50:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_evaluasi_2`
--

CREATE TABLE `tbl_hasil_evaluasi_2` (
  `id` bigint UNSIGNED NOT NULL,
  `hasil` bigint NOT NULL,
  `hasil_2` bigint NOT NULL,
  `hasil_3` bigint NOT NULL,
  `hasil_4` bigint NOT NULL,
  `hasil_5` bigint NOT NULL,
  `hasil_6` bigint NOT NULL,
  `hasil_7` bigint NOT NULL,
  `hasil_8` bigint NOT NULL,
  `hasil_9` bigint NOT NULL,
  `hasil_10` bigint NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_evaluasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_hasil_evaluasi_2`
--

INSERT INTO `tbl_hasil_evaluasi_2` (`id`, `hasil`, `hasil_2`, `hasil_3`, `hasil_4`, `hasil_5`, `hasil_6`, `hasil_7`, `hasil_8`, `hasil_9`, `hasil_10`, `noreg`, `nama_evaluasi`, `nim`, `jurusan`, `created_at`, `updated_at`) VALUES
(1, 5, 4, 3, 2, 1, 2, 3, 3, 3, 4, 'EKDOM-308112', 'Mohammad Syamsul Ma\'rif', 'F1B118038', 'fisika fakultas MIPA', '2021-09-13 17:50:40', '2021-09-13 17:50:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_evaluasi_prasarana`
--

CREATE TABLE `tbl_hasil_evaluasi_prasarana` (
  `id` bigint UNSIGNED NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_hasil_evaluasi_prasarana`
--

INSERT INTO `tbl_hasil_evaluasi_prasarana` (`id`, `noreg`, `kategori`, `nim`, `jurusan`, `saran`, `created_at`, `updated_at`) VALUES
(1, 'EKDOM-308112', 'evaluasi prasarana', 'F1B118038', 'fisika fakultas MIPA', 'kasih perketat lahan parkir', '2021-09-13 17:51:18', '2021-09-13 17:51:18'),
(2, 'EKDOM-413507', 'evaluasi prasarana', '19216811192834', 'sastra prancis fib uho', 'kalo bisa mobil saya di parkirkan juga', '2021-09-13 19:48:53', '2021-09-13 19:48:53'),
(3, 'EKDOM-548005', 'evaluasi prasarana', '19216811192834', 'fisika fakultas MIPA', 'perbaiki jaringan', '2021-09-13 20:04:01', '2021-09-13 20:04:01'),
(4, 'EKDOM-253976', 'evaluasi prasarana', '19216811192834', 'fisika fakultas MIPA', 'wifi di kasih cepat', '2021-09-13 20:43:57', '2021-09-13 20:43:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_evaluasi_prasarana_2`
--

CREATE TABLE `tbl_hasil_evaluasi_prasarana_2` (
  `id` bigint UNSIGNED NOT NULL,
  `hasil` bigint NOT NULL,
  `hasil_2` bigint NOT NULL,
  `hasil_3` bigint NOT NULL,
  `hasil_4` bigint NOT NULL,
  `hasil_5` bigint NOT NULL,
  `hasil_6` bigint NOT NULL,
  `hasil_7` bigint NOT NULL,
  `hasil_8` bigint NOT NULL,
  `hasil_9` bigint NOT NULL,
  `hasil_10` bigint NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_evaluasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_hasil_evaluasi_prasarana_2`
--

INSERT INTO `tbl_hasil_evaluasi_prasarana_2` (`id`, `hasil`, `hasil_2`, `hasil_3`, `hasil_4`, `hasil_5`, `hasil_6`, `hasil_7`, `hasil_8`, `hasil_9`, `hasil_10`, `noreg`, `nim`, `jurusan`, `nama_evaluasi`, `created_at`, `updated_at`) VALUES
(1, 5, 4, 4, 5, 5, 2, 2, 3, 3, 4, 'EKDOM-308112', 'F1B118038', 'fisika fakultas MIPA', 'Mohammad Syamsul Ma\'rif', '2021-09-13 17:51:18', '2021-09-13 17:51:18'),
(2, 5, 4, 3, 2, 1, 2, 3, 4, 5, 4, 'EKDOM-413507', '19216811192834', 'sastra prancis fib uho', 'dosenekdom', '2021-09-13 19:48:53', '2021-09-13 19:48:53'),
(3, 5, 4, 3, 2, 1, 2, 2, 3, 4, 5, 'EKDOM-548005', '19216811192834', 'fisika fakultas MIPA', 'Dr.eng.laagusu.S.Si.,M.Si', '2021-09-13 20:04:01', '2021-09-13 20:04:01'),
(4, 1, 2, 2, 3, 4, 4, 4, 5, 5, 4, 'EKDOM-253976', '19216811192834', 'fisika fakultas MIPA', 'Lina Lestari.,S.Pd.,M.Si', '2021-09-13 20:43:57', '2021-09-13 20:43:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_evaluasi_staf`
--

CREATE TABLE `tbl_hasil_evaluasi_staf` (
  `id` bigint UNSIGNED NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saran` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_hasil_evaluasi_staf`
--

INSERT INTO `tbl_hasil_evaluasi_staf` (`id`, `noreg`, `kategori`, `nim`, `jurusan`, `saran`, `created_at`, `updated_at`) VALUES
(1, 'EKDOM-413507', 'evaluasi staf', '19216811192834', 'sastra prancis fib uho', 'staf lebih cepat lagi dalam bekerja', '2021-09-13 19:38:13', '2021-09-13 19:38:13'),
(3, 'EKDOM-548005', 'evaluasi staf', '19216811192834', 'fisika fakultas MIPA', 'lainkali beban saya jg', '2021-09-13 19:58:49', '2021-09-13 19:58:49'),
(4, 'EKDOM-253976', 'evaluasi staf', '19216811192834', 'fisika fakultas MIPA', 'staf lebih rajin lagi', '2021-09-13 20:43:14', '2021-09-13 20:43:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_evaluasi_staf_2`
--

CREATE TABLE `tbl_hasil_evaluasi_staf_2` (
  `id` bigint UNSIGNED NOT NULL,
  `hasil` bigint NOT NULL,
  `hasil_2` bigint NOT NULL,
  `hasil_3` bigint NOT NULL,
  `hasil_4` bigint NOT NULL,
  `hasil_5` bigint NOT NULL,
  `hasil_6` bigint NOT NULL,
  `hasil_7` bigint NOT NULL,
  `hasil_8` bigint NOT NULL,
  `hasil_9` bigint NOT NULL,
  `hasil_10` bigint NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_evaluasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_hasil_evaluasi_staf_2`
--

INSERT INTO `tbl_hasil_evaluasi_staf_2` (`id`, `hasil`, `hasil_2`, `hasil_3`, `hasil_4`, `hasil_5`, `hasil_6`, `hasil_7`, `hasil_8`, `hasil_9`, `hasil_10`, `noreg`, `nim`, `jurusan`, `nama_evaluasi`, `created_at`, `updated_at`) VALUES
(1, 5, 4, 4, 4, 3, 4, 5, 4, 3, 1, 'EKDOM-413507', '19216811192834', 'sastra prancis fib uho', 'dosenekdom', '2021-09-13 19:38:13', '2021-09-13 19:38:13'),
(3, 5, 4, 3, 2, 2, 1, 3, 4, 4, 5, 'EKDOM-548005', '19216811192834', 'fisika fakultas MIPA', 'Dr.eng.laagusu.S.Si.,M.Si', '2021-09-13 19:58:49', '2021-09-13 19:58:49'),
(4, 5, 4, 3, 2, 1, 2, 3, 4, 5, 4, 'EKDOM-253976', '19216811192834', 'fisika fakultas MIPA', 'Lina Lestari.,S.Pd.,M.Si', '2021-09-13 20:43:14', '2021-09-13 20:43:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_mahasiswa`
--

CREATE TABLE `tbl_mahasiswa` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_mahasiswa`
--

INSERT INTO `tbl_mahasiswa` (`id`, `name`, `noreg`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Mohammad Syamsul Ma\'rif', 'EKDOM-411487', 'mahasiswa', '2021-09-13 17:21:57', '2021-09-13 17:21:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pertanyaan`
--

CREATE TABLE `tbl_pertanyaan` (
  `id` bigint UNSIGNED NOT NULL,
  `pertanyaan` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_pertanyaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_pertanyaan`
--

INSERT INTO `tbl_pertanyaan` (`id`, `pertanyaan`, `noreg`, `code_pertanyaan`, `created_at`, `updated_at`) VALUES
(1, 'Menjaga kuliah agar tetap berada pada konteks topic yang di\r\nbicarakan', 'EKDOM-209421', 'ekdom0001', '2021-09-13 17:43:09', '2021-09-13 17:43:09'),
(2, 'Ketepatan waktu memulai kuliah', 'EKDOM-209421', 'ekdom0002', '2021-09-13 17:43:23', '2021-09-13 17:43:23'),
(3, 'Ketepatan waktu mengakhiri kuliah', 'EKDOM-209421', 'ekdom0003', '2021-09-13 17:43:30', '2021-09-13 17:43:30'),
(4, 'Penampilan / performance', 'EKDOM-209421', 'ekdom0004', '2021-09-13 17:43:38', '2021-09-13 17:43:38'),
(5, 'Penilaian secara keseluruhan terhadap dosen yang bersangkutan', 'EKDOM-209421', 'ekdom0005', '2021-09-13 17:43:46', '2021-09-13 17:43:46'),
(6, 'Kejelasan tujuan dari mata kuliah', 'EKDOM-209421', 'ekdom0006', '2021-09-13 17:43:55', '2021-09-13 17:43:55'),
(7, 'Kesesuaian komposisi materi dengan kuliah', 'EKDOM-209421', 'ekdom0007', '2021-09-13 17:44:02', '2021-09-13 17:44:02'),
(8, 'Pengorganisasian sistematika materi kuliah', 'EKDOM-209421', 'ekdom0008', '2021-09-13 17:44:10', '2021-09-13 17:44:10'),
(9, 'Keseimbangan tugas (kasus/latihan) dengan materi kuliah', 'EKDOM-209421', 'ekdom0009', '2021-09-13 17:44:17', '2021-09-13 17:44:17'),
(10, 'Pengusaan materi kuliah', 'EKDOM-209421', 'ekdom0010', '2021-09-13 17:44:28', '2021-09-13 17:44:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pertanyaan_prasarana`
--

CREATE TABLE `tbl_pertanyaan_prasarana` (
  `id` bigint UNSIGNED NOT NULL,
  `pertanyaan` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_pertanyaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_pertanyaan_prasarana`
--

INSERT INTO `tbl_pertanyaan_prasarana` (`id`, `pertanyaan`, `noreg`, `code_pertanyaan`, `created_at`, `updated_at`) VALUES
(1, 'wifi di fakultas aman ji gak di hack', 'EKDOM-209421', 'ekdom0001', '2021-09-13 17:44:50', '2021-09-13 17:44:50'),
(2, 'parkiran luas', 'EKDOM-209421', 'ekdom0002', '2021-09-13 17:44:58', '2021-09-13 17:44:58'),
(3, 'taman indah dan bersih', 'EKDOM-209421', 'ekdom0003', '2021-09-13 17:45:10', '2021-09-13 17:45:10'),
(4, 'lokasi atau tata letak fakultas yang tidak menyulitkan anda untuk mencari ruangan', 'EKDOM-209421', 'ekdom0004', '2021-09-13 17:45:28', '2021-09-13 17:45:28'),
(5, 'kinerja satpam yang ok setiap harinya', 'EKDOM-209421', 'ekdom0005', '2021-09-13 17:45:57', '2021-09-13 17:45:57'),
(6, 'resource perpustakaan yang memadai dalam hal buku-buku dan referensi', 'EKDOM-209421', 'ekdom0006', '2021-09-13 17:46:36', '2021-09-13 17:46:36'),
(7, 'ruang kuliah yang bagus dan rapih', 'EKDOM-209421', 'ekdom0007', '2021-09-13 17:48:45', '2021-09-13 17:48:45'),
(8, 'kursi dan meja yang tertata rapih', 'EKDOM-209421', 'ekdom0008', '2021-09-13 17:49:01', '2021-09-13 17:49:01'),
(9, 'listrik di fakultas tidak pernah mati', 'EKDOM-209421', 'ekdom0009', '2021-09-13 17:49:15', '2021-09-13 17:49:15'),
(10, 'fakultas menyediakan sarana untuk tempat belajar bersama', 'EKDOM-209421', 'ekdom0010', '2021-09-13 17:49:44', '2021-09-13 17:49:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pertanyaan_staf`
--

CREATE TABLE `tbl_pertanyaan_staf` (
  `id` bigint UNSIGNED NOT NULL,
  `pertanyaan` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_pertanyaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tbl_pertanyaan_staf`
--

INSERT INTO `tbl_pertanyaan_staf` (`id`, `pertanyaan`, `noreg`, `code_pertanyaan`, `created_at`, `updated_at`) VALUES
(1, 'Selalu Menyelesaikan pekerjaan yang dibebankan oleh mahasiswa', 'EKDOM-209421', 'ekdom0001', '2021-09-13 18:17:38', '2021-09-13 18:17:51'),
(2, 'memiliki skill/keahlian untuk menyelesaikan pekerjaan yang dibebankan kepada saya', 'EKDOM-209421', 'ekdom0002', '2021-09-13 18:18:01', '2021-09-13 18:18:01'),
(3, 'Jiwa kepemimpinan', 'EKDOM-209421', 'ekdom0003', '2021-09-13 18:18:09', '2021-09-13 18:18:09'),
(4, 'Tidak Membuat kesalahan dalam menyelasaikan tugas', 'EKDOM-209421', 'ekdom0004', '2021-09-13 18:18:16', '2021-09-13 18:18:16'),
(5, 'Mandiri dalam bekerja', 'EKDOM-209421', 'ekdom0005', '2021-09-13 18:18:22', '2021-09-13 18:18:22'),
(6, 'Jujur saat bekerja dan saat diberikan amanah', 'EKDOM-209421', 'ekdom0006', '2021-09-13 18:18:30', '2021-09-13 18:18:30'),
(7, 'Dalam menyelesaikan tugas saya dapat menyelsaikan dengan tepat  dan cepat sesuai waktu yang ditentukan', 'EKDOM-209421', 'ekdom0007', '2021-09-13 18:18:37', '2021-09-13 18:18:37'),
(8, 'Memiliki komitmen terhadap organisasi', 'EKDOM-209421', 'ekdom0008', '2021-09-13 18:18:45', '2021-09-13 18:18:45'),
(9, 'Bekerja sama dengan baik dalam organisasi', 'EKDOM-209421', 'ekdom0009', '2021-09-13 18:18:52', '2021-09-13 18:18:52'),
(10, 'Memiliki tanggung jawab terhadap organisasi', 'EKDOM-209421', 'ekdom0010', '2021-09-13 18:18:59', '2021-09-13 18:18:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hobi` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_wa` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Nim_Nidn_Nip` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jurusan` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biodata` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `role` enum('administrator','dosen','mahasiswa') COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluasi` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `prasarana` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `staf` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `noreg`, `hobi`, `alamat`, `no_wa`, `about`, `foto_profile`, `Nim_Nidn_Nip`, `jurusan`, `biodata`, `email_verified_at`, `role`, `evaluasi`, `prasarana`, `staf`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'adminekdom', 'adminekdom@gmail.com', 'EKDOM-209421', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'administrator', 'Y', 'Y', 'Y', '$2y$10$saDUXBZS9mdO7lOPXdNeAueG2Gtn4A7lTQnI0TyFKBBAiawTx380i', NULL, NULL, NULL),
(2, 'Mohammad Syamsul Ma\'rif', 'samsul.marif100@gmail.com', 'EKDOM-308112', 'coding laravel, VUE JS, dan lain-lain', 'Jalan Jambu Mente', '081804228935', 'gas terus', NULL, 'F1B118038', 'fisika fakultas MIPA', 'Y', NULL, 'mahasiswa', 'Y', 'Y', 'N', '$2y$10$7GBJHkypGV9s5AT4Q2eHWeiBFrF4XLeOdyrZazGSDxd5llwd9fhve', NULL, '2021-09-13 17:21:57', '2021-09-13 17:25:30'),
(3, 'dosenekdom', 'dosen@gmail.com', 'EKDOM-413507', 'mengajar di fib', 'jalan jambu mente', '081802442221212', 'baca buku', NULL, '19216811192834', 'sastra prancis fib uho', 'Y', NULL, 'dosen', 'N', 'Y', 'Y', '$2y$10$rVmJfg/icf8/1jaH7stRKOBmNnkmbHjPE8jfueo2pcM1qCIxYvdcy', NULL, '2021-09-13 17:57:21', '2021-09-13 19:20:21'),
(4, 'Dr.eng.laagusu.S.Si.,M.Si', 'laagusu@gmail.com', 'EKDOM-548005', 'kerja soal mekanika kuantum', 'jalan hea mokodompit', '08184123123123', 'belajar terus nak kapan wisudanya', NULL, '19216811192834', 'fisika fakultas MIPA', 'Y', NULL, 'dosen', 'N', 'Y', 'Y', '$2y$10$Zuch.CTxRa4tVg0cdRLyluve0s0HuihgU9qsF8nNa3enXGHsZTfyO', NULL, '2021-09-13 19:52:37', '2021-09-13 19:56:44'),
(5, 'Lina Lestari.,S.Pd.,M.Si', 'linalestari@gmail.com', 'EKDOM-253976', 'belajar fismat', 'jalan sumber sari', '0812318238123', 'gas terus nak', NULL, '19216811192834', 'fisika fakultas MIPA', 'Y', NULL, 'dosen', 'N', 'Y', 'Y', '$2y$10$MaMRXLVzZtFVmxBb8Ef20el0w1fTPSAWuKsgIIqfS/E8lzgH6Rdle', NULL, '2021-09-13 20:05:05', '2021-09-13 20:06:02');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `tbl_dosen`
--
ALTER TABLE `tbl_dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_evaluasi`
--
ALTER TABLE `tbl_evaluasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_evaluasi_prasarana`
--
ALTER TABLE `tbl_evaluasi_prasarana`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_evaluasi_staf`
--
ALTER TABLE `tbl_evaluasi_staf`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hasil_evaluasi`
--
ALTER TABLE `tbl_hasil_evaluasi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hasil_evaluasi_2`
--
ALTER TABLE `tbl_hasil_evaluasi_2`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hasil_evaluasi_prasarana`
--
ALTER TABLE `tbl_hasil_evaluasi_prasarana`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hasil_evaluasi_prasarana_2`
--
ALTER TABLE `tbl_hasil_evaluasi_prasarana_2`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hasil_evaluasi_staf`
--
ALTER TABLE `tbl_hasil_evaluasi_staf`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_hasil_evaluasi_staf_2`
--
ALTER TABLE `tbl_hasil_evaluasi_staf_2`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pertanyaan`
--
ALTER TABLE `tbl_pertanyaan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pertanyaan_prasarana`
--
ALTER TABLE `tbl_pertanyaan_prasarana`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_pertanyaan_staf`
--
ALTER TABLE `tbl_pertanyaan_staf`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT untuk tabel `tbl_dosen`
--
ALTER TABLE `tbl_dosen`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_evaluasi`
--
ALTER TABLE `tbl_evaluasi`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_evaluasi_prasarana`
--
ALTER TABLE `tbl_evaluasi_prasarana`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_evaluasi_staf`
--
ALTER TABLE `tbl_evaluasi_staf`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_evaluasi`
--
ALTER TABLE `tbl_hasil_evaluasi`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_evaluasi_2`
--
ALTER TABLE `tbl_hasil_evaluasi_2`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_evaluasi_prasarana`
--
ALTER TABLE `tbl_hasil_evaluasi_prasarana`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_evaluasi_prasarana_2`
--
ALTER TABLE `tbl_hasil_evaluasi_prasarana_2`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_evaluasi_staf`
--
ALTER TABLE `tbl_hasil_evaluasi_staf`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_hasil_evaluasi_staf_2`
--
ALTER TABLE `tbl_hasil_evaluasi_staf_2`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tbl_mahasiswa`
--
ALTER TABLE `tbl_mahasiswa`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_pertanyaan`
--
ALTER TABLE `tbl_pertanyaan`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_pertanyaan_prasarana`
--
ALTER TABLE `tbl_pertanyaan_prasarana`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_pertanyaan_staf`
--
ALTER TABLE `tbl_pertanyaan_staf`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
