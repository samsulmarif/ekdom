-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 08 Sep 2021 pada 03.54
-- Versi server: 8.0.26-0ubuntu0.20.04.2
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekdom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noreg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hobi` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_wa` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Nim_Nidn_Nip` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biodata` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `role` enum('administrator','dosen','mahasiswa') COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluasi` enum('N','Y') COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `noreg`, `hobi`, `alamat`, `no_wa`, `about`, `foto_profile`, `Nim_Nidn_Nip`, `biodata`, `email_verified_at`, `role`, `evaluasi`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'adminekdom', 'adminekdom@gmail.com', 'EKDOM-100163', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'administrator', 'N', '$2y$10$dmYjL0QdIJZZQaG0fcbjlOETptlpg7hUZKXu1KHyH0ZGbLF7r4yX.', NULL, NULL, NULL),
(2, 'arwan prianto mangidi', 'arwan@gmail.com', 'EKDOM-231684', 'coding python', 'jalan konawe', '081804228935', 'jangan bosan belajar', NULL, 'F1B118038', 'Y', NULL, 'mahasiswa', 'N', '$2y$10$5qCRodA7olKjMRjThYeGk.DD8CHgJS70LiPeyZMz/cw/LVqp/AQjG', NULL, '2021-09-07 04:21:24', '2021-09-07 05:40:13'),
(3, 'Prof.dr.dosenekdom.SSi.,M.Si', 'dosen@gmail.com', 'EKDOM-932321', 'mengajar di fib', 'jalan sma 2 kendari', '081804228935', 'jangan lupa susun tugas akhirnya yaa', NULL, '19216811192834', 'Y', NULL, 'dosen', 'N', '$2y$10$aOs9Xl0aOkv4xbHxpDxS5eiUtLJcHUxSNZoX.f564d25aZnHEC582', NULL, '2021-09-07 04:42:52', '2021-09-07 05:37:54'),
(7, 'muhammad syamsul marif', 'samsul.marif100@gmail.com', 'EKDOM-047864', 'coding', 'jalan sma 2 kendari', '081804228935', 'gas poll', NULL, 'F1B118004', 'Y', NULL, 'mahasiswa', 'N', '$2y$10$SLsRMhGqg6rBa8XB8KNVae1c9IWuVW1h1PQBo7f/WxD8iNhD56Hji', NULL, '2021-09-07 05:57:29', '2021-09-07 06:00:26'),
(8, 'MIftahul Jannah', 'miftah@gmail.com', 'EKDOM-265779', NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'mahasiswa', 'N', '$2y$10$.0sK/7ckkHQl.jDixSTqEONFTeEPMQtDL9qxhutwjOEVu/fk6rmI6', NULL, '2021-09-07 06:05:31', '2021-09-07 06:05:31');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
