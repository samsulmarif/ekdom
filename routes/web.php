<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\mahasiswa\ProfileController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Routes FrontEnd
Route::get('/',[\App\Http\Controllers\depan\DashboardDepan::class , 'index']);


/** routes group buat multi login dan hak akses users  */

/** routes group Admin */
Route::prefix('admin')->group(function () {
    /** routes users managemen */
    Route::get('/dashboard', [DashboardController::class, 'DashboardAdmin'])->name('DashboardAdmin')->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/users', [UsersController::class, 'index'])->name('view-users')->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/mahasiswa', [UsersController::class, 'mahasiswa'])->name('view-mahasiswa')->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/dosen', [UsersController::class, 'dosen'])->name('view-dosen')->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/users-forms', [UsersController::class, 'create'])->name('forms-tambah-users')->middleware(['roledosen', 'rolemahasiswa']);
    Route::post('/users-tambah-data', [UsersController::class, 'store'])->name('kirim-data-users')->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/users/{modelsUsers}/formsEdit', [UsersController::class, 'edit'])->name('view-edit-data-users')->middleware(['roledosen', 'rolemahasiswa']);
    Route::put('/users/{modelsUsers}/edit-data', [UsersController::class, 'update'])->name('update-data-users')->middleware(['roledosen', 'rolemahasiswa']);
    Route::delete('/users/{modelsUsers}/delete', [UsersController::class, 'destroy'])->name('delete-data-users')->middleware(['roledosen', 'rolemahasiswa']);
    /** end:: routes users managemen */

    /** evaluasi kinerja dosen */
    Route::get('/kinerja_dosen', [App\Http\Controllers\admin\EvaluasiController::class, 'create'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/evaluasi', [\App\Http\Controllers\admin\EvaluasiController::class, 'index_evaluasi'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/pertanyaan_evaluasi', [\App\Http\Controllers\admin\EvaluasiController::class, 'index_pertanyaan'])->middleware(['roledosen', 'rolemahasiswa']);
    //evaluasi
    Route::get('/tambah_evaluasi', [\App\Http\Controllers\admin\EvaluasiController::class, 'create_evaluasi'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::post('/insert_evaluasi', [\App\Http\Controllers\admin\EvaluasiController::class, 'store_evaluasi'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/evaluasi/{evaluasi}/edit', [\App\Http\Controllers\admin\EvaluasiController::class, 'edit_evaluasi'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::put('/evaluasi/{update}/update', [\App\Http\Controllers\admin\EvaluasiController::class, 'update_evaluasi'])->middleware(['roledosen', 'rolemahasiswa']);
    //pertanyaan
    Route::get('/tambah_pertanyaan', [\App\Http\Controllers\admin\EvaluasiController::class, 'create_pertanyaan'])->middleware(['roledosen', 'rolemahasiswa', 'evaluasi']);
    Route::post('/insert_pertanyaan', [\App\Http\Controllers\admin\EvaluasiController::class, 'store_pertanyaan'])->middleware(['roledosen', 'rolemahasiswa', 'evaluasi']);
    Route::get('/pertanyaan/{edit}/edit', [\App\Http\Controllers\admin\EvaluasiController::class, 'edit_pertanyaan'])->middleware(['roledosen', 'rolemahasiswa', 'evaluasi']);
    Route::put('/pertanyaan/{update}/update', [\App\Http\Controllers\admin\EvaluasiController::class, 'update_pertanyaan'])->middleware(['roledosen', 'rolemahasiswa', 'evaluasi']);
    Route::delete('/pertanyaan/{delete}/delete', [\App\Http\Controllers\admin\EvaluasiController::class, 'delete_pertanyaan'])->middleware(['roledosen', 'rolemahasiswa', 'evaluasi']);
    /** end routes evaluasi kinerja dosen oleh mahasiswa */

    /** routes untuk prasarana */
    Route::get('/prasarana',[\App\Http\Controllers\admin\EvaluasiController::class , 'index_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/pertanyaan_prasarana',[\App\Http\Controllers\admin\EvaluasiController::class , 'index_pertanyaan_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/prasarana/{edit}/edit',[\App\Http\Controllers\admin\EvaluasiController::class , 'edit_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::put('/prasarana/{update}/update',[\App\Http\Controllers\admin\EvaluasiController::class , 'update_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/tambah_prasarana',[\App\Http\Controllers\admin\EvaluasiController::class , 'create_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/tambah_pertanyaan_prasarana',[\App\Http\Controllers\admin\EvaluasiController::class , 'create_pertanyaan_prasarana'])->middleware(['roledosen', 'rolemahasiswa','StatusPrasarana']);
    Route::post('/insert_prasarana',[\App\Http\Controllers\admin\EvaluasiController::class , 'store_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::post('/insert_pertanyaan_prasarana',[\App\Http\Controllers\admin\EvaluasiController::class , 'store_pertanyaan_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);


    Route::get('/pertanyaan_prasarana/{edit}/edit',[\App\Http\Controllers\admin\EvaluasiController::class , 'edit_pertanyaan_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::put('/pertanyaan_prasarana/{update}/update',[\App\Http\Controllers\admin\EvaluasiController::class , 'update_pertanyaan_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/pertanyaan_prasarana/{delete}/delete',[\App\Http\Controllers\admin\EvaluasiController::class , 'delete_pertanyaan_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/hasil_evaluasi_prasarana',[\App\Http\Controllers\admin\HasilEvaluasiPrasarana::class , 'hasil_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/hasil_evaluasi_mahasiswa/{view}/view',[\App\Http\Controllers\admin\HasilEvaluasiPrasarana::class , 'hasil_evaluasi_prasarana'])->middleware(['roledosen', 'rolemahasiswa']);
    /** END::routes untuk prasarana */

    /** routes evaluasi staf */
    Route::get('/staf',[App\Http\Controllers\admin\EvaluasiController::class ,'index_staf'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/pertanyaan_staf',[\App\Http\Controllers\admin\EvaluasiController::class , 'index_pertanyaan_staf'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/tambah_staf',[\App\Http\Controllers\admin\EvaluasiController::class , 'create_evaluasi_staf'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/tambah_pertanyaan_staf',[\App\Http\Controllers\admin\EvaluasiController::class , 'create_pertanyaan_staf'])->middleware(['roledosen', 'rolemahasiswa','StatusStaf']);

    Route::post('/insert_staf',[\App\Http\Controllers\admin\EvaluasiController::class , 'store_evaluasi_staf'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::post('/insert_staf_pertanyaan',[\App\Http\Controllers\admin\EvaluasiController::class , 'store_pertanyaan_staf'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/staf/{edit}/edit',[\App\Http\Controllers\admin\EvaluasiController::class ,'edit_evaluasi_staf'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/staf_pertanyaan/{edit}/edit',[\App\Http\Controllers\admin\EvaluasiController::class , 'edit_pertanyaan_staf'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::put('/insert_staf/{update}/update',[\App\Http\Controllers\admin\EvaluasiController::class , 'update_evaluasi_staf'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::put('/insert_pertanyaan_staf/{update}/update',[\App\Http\Controllers\admin\EvaluasiController::class , 'update_pertanyaan_staf'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::delete('/delete_pertanyaan_staf/{delete}/delete',[\App\Http\Controllers\admin\EvaluasiController::class, 'delete_pertanyaan_staf'])->middleware(['roledosen', 'rolemahasiswa']);

    Route::get('/hasil_evaluasi_staf',[App\Http\Controllers\admin\HasilEvaluasiStaf::class , 'index'])->middleware(['roledosen', 'rolemahasiswa']);
    Route::get('/hasil_evaluasi_staf/{view}/view',[\App\Http\Controllers\admin\HasilEvaluasiStaf::class , 'view'])->middleware(['roledosen', 'rolemahasiswa']);
    /** end:: routes evaluasi staf */

    // routes time line mahasiswa
    Route::get('/lacak_status/{status_mahasiswa}/mahasiswa',[\App\Http\Controllers\UsersController::class , 'show'])->middleware(['roledosen', 'rolemahasiswa']);

    // Routes import data mahasiswa dan dosen
});

/** routes group Dosen */
Route::prefix('dosen')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'DashboardDosen'])->name('dashboard-dosen')->middleware(['roleadmin', 'rolemahasiswa']);
    Route::get('/profile/view', [\App\Http\Controllers\dosen\ProfileController::class, 'profile_dosen'])->middleware(['roleadmin', 'rolemahasiswa']);
    Route::get('/profile/{edit}/dosen', [\App\Http\Controllers\dosen\ProfileController::class, 'get_data_dosen'])->middleware(['roleadmin', 'rolemahasiswa']);
    Route::put('/profile/{update}/dosen', [\App\Http\Controllers\dosen\ProfileController::class, 'update_data_profile'])->middleware(['roleadmin', 'rolemahasiswa']);

    Route::get('/reset_password', [\App\Http\Controllers\dosen\ProfileController::class, 'reset_pass'])->middleware(['roleadmin', 'rolemahasiswa']);
    Route::put('/reset_pass/{passupdate}/ubah', [\App\Http\Controllers\dosen\ProfileController::class, 'update_pass'])->middleware(['roleadmin', 'rolemahasiswa']);

    // /** NEXT PROJECT UNTUK PENGEMBANAGAN EKDOM (SETIAP DOSEN BISA MEMBUAT SENDIRI UNTUK PERTANYAAN EVALUASINYA)  */
    // /** routes dosen dibawah ini untuk kategori evaluasi */
    // Route::get('/evaluasi/index', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'index_evaluasi'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::get('/evaluasi', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'create_evaluasi'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::post('/insert_evaluasi', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'store_evaluasi'])->middleware(['roleadmin', 'rolemahasiswa']);

    // /** routes dosen dibawah ini untuk pertayaan evaluasi */
    // Route::get('/pertanyaan_evaluasi', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'index'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::get('/tambah_pertanyaan', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'create'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::post('/insert_pertanyaan', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'store'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::get('/pertanyaan/{edit}/edit', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'edit'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::put('/pertanyaan/{update}/update', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'update'])->middleware(['roleadmin', 'rolemahasiswa']);
    // Route::delete('/pertanyaan/{delete}/delete', [\App\Http\Controllers\dosen\PertanyaanEvaluasi::class, 'delete'])->middleware(['roleadmin', 'rolemahasiswa']);
    // /** // NEXT PROJECT UNTUK PENGEMBANAGAN EKDOM (SETIAP DOSEN BISA MEMBUAT SENDIRI DARI PERTANYAAN EVALUASINYA)  */

    /** lihat hasil evaluasi yang di kirim oleh mahasiswa */
    Route::get('/hasil_evaluasi_mahasiswa',[\App\Http\Controllers\dosen\HasilEvaluasi::class , 'hasil_evaluasi_mahasiswa'])->middleware(['roleadmin', 'rolemahasiswa']);
    Route::get('/hasil_evaluasi_mahasiswa/{view}/view',[\App\Http\Controllers\dosen\HasilEvaluasi::class , 'lihat_hasil_evaluasi'])->middleware(['roleadmin', 'rolemahasiswa']);

    /** routes kirim evauasi staf dan prasarana */
    Route::get('/evaluasi',[\App\Http\Controllers\dosen\staf\EvaluasiController::class , 'index'])->middleware(['roleadmin', 'rolemahasiswa','ProfileDosen']);
    Route::get('/kirim_evaluasi_staf',[App\Http\Controllers\dosen\staf\EvaluasiController::class , 'evaluasi_staf'])->middleware(['roleadmin', 'rolemahasiswa','StatusEvaluasiStaf','ProfileDosen']);
    Route::get('/kirim_evaluasi_prasarana',[\App\Http\Controllers\dosen\staf\EvaluasiController::class , 'evaluasi_prasarana'])->middleware(['roleadmin', 'rolemahasiswa','StatusPrasaranStaf','ProfileDosen']);

    Route::post('/insert_evaluasi_staf',[App\Http\Controllers\dosen\staf\EvaluasiController::class , 'insert_evaluasi_staf'])->middleware(['roleadmin', 'rolemahasiswa']);
    Route::post('/insert_evaluasi_prasarana',[\App\Http\Controllers\dosen\staf\EvaluasiController::class , 'insert_evaluasi_prasarana'])->middleware(['roleadmin', 'rolemahasiswa']);
});

/** routes group Mahasiswa */
Route::prefix('mahasiswa')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'DashboardMahasiswa'])->name('mahasiswa-dashboard')->middleware(['roleadmin', 'roledosen']);
    Route::get('/profile/view', [\App\Http\Controllers\mahasiswa\ProfileController::class, 'profile'])->middleware(['roleadmin', 'roledosen']);
    Route::get('/profile/{edit}/mahasiswa', [\App\Http\Controllers\mahasiswa\ProfileController::class, 'edit_profile'])->middleware(['roleadmin', 'roledosen']);
    Route::put('/profile/{update}/mahasiswa', [\App\Http\Controllers\mahasiswa\ProfileController::class, 'update_profile'])->middleware(['roleadmin', 'roledosen']);

    Route::get('/reset_password', [\App\Http\Controllers\mahasiswa\ProfileController::class, 'resetPassword'])->middleware(['roleadmin', 'roledosen']);
    Route::put('/reset_pass/{pass}/ubah', [\App\Http\Controllers\mahasiswa\ProfileController::class, 'ubahPassword'])->middleware(['roleadmin', 'roledosen']);

    /** routes kirim evaluasi prasarana dan kinerja dosen */
    Route::get('/evaluasi', [\App\Http\Controllers\mahasiswa\EvaluasiController::class, 'index'])->middleware(['roleadmin', 'roledosen', 'roleevaluasi']);
    Route::get('/kirim_evaluasi', [\App\Http\Controllers\mahasiswa\EvaluasiController::class, 'evaluasi'])->middleware(['roleadmin', 'roledosen', 'roleevaluasi','evaluasifinish']);
    Route::get('/kirim_evaluasi_prasarana',[\App\Http\Controllers\mahasiswa\EvaluasiController::class, 'prasarana'])->middleware(['roleadmin', 'roledosen', 'roleevaluasi','prasarana']);

    Route::post('/insert_evaluasi_prasarana',[\App\Http\Controllers\mahasiswa\EvaluasiController::class, 'insert_evaluasi_prasarana'])->middleware(['roleadmin', 'roledosen', 'roleevaluasi']);;
    Route::post('/insert_evaluasi', [\App\Http\Controllers\mahasiswa\EvaluasiController::class, 'insert_evaluasi'])->middleware(['roleadmin', 'roledosen', 'roleevaluasi']);

    /* routes hasil evaluasi saya*/
    Route::get('evaluasi-saya-dosen',[App\Http\Controllers\mahasiswa\EvaluasiController::class , 'hasil_evaluasi'])->middleware(['roleadmin', 'roledosen', 'roleevaluasi']);
});

require __DIR__ . '/auth.php';
