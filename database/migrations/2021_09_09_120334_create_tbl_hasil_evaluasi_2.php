<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblHasilEvaluasi2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_hasil_evaluasi_2', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('hasil');
            $table->bigInteger('hasil_2');
            $table->bigInteger('hasil_3');
            $table->bigInteger('hasil_4');
            $table->bigInteger('hasil_5');
            $table->bigInteger('hasil_6');
            $table->bigInteger('hasil_7');
            $table->bigInteger('hasil_8');
            $table->bigInteger('hasil_9');
            $table->bigInteger('hasil_10');
            $table->string('noreg');
            $table->string('nama_evaluasi');
            $table->string('nim');
            $table->string('jurusan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_hasil_evaluasi_2');
    }
}
