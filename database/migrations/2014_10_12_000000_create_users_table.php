<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Keygen\Keygen;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('noreg')->nullable();
            $table->string('hobi', 128)->nullable();
            $table->string('alamat', 128)->nullable();
            $table->string('no_wa', 50)->nullable();
            $table->string('about', 128)->nullable();
            $table->string('foto_profile')->nullable();
            $table->string('Nim_Nidn_Nip', 128)->nullable();
            $table->string('jurusan', 128)->nullable();
            $table->enum('biodata', ['N', 'Y']);
            $table->timestamp('email_verified_at')->nullable();
            $table->enum('role', ['administrator', 'dosen', 'mahasiswa']);
            $table->enum('evaluasi', ['N', 'Y']);
            $table->enum('prasarana', ['N', 'Y']);
            $table->enum('staf', ['N', 'Y']);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'name' => 'adminekdom',
            'email' => 'adminekdom@gmail.com',
            'role' => 'administrator',
            'password' => Hash::make('adminekdom!@#'),
            'noreg' => Keygen::numeric(12)->prefix('EKDOM-')->generate()
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
