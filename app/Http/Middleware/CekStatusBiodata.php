<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CekStatusBiodata
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->biodata == 'N') {
            return redirect('mahasiswa/profile/view')->with('gagal', 'tidak bisa kirim evaluasi karena biodata anda belum lengkap , harap lengkapi bioadata');
        }
        return $next($request);
    }
}
