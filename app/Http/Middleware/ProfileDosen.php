<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileDosen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->biodata == 'N') {
            return redirect('dosen/profile/view')->with('gagal','harap lengkapi biodata jika ingin mengirim evaluasi');
        }
        return $next($request);
    }
}
