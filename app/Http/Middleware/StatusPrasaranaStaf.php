<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatusPrasaranaStaf
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::user()->prasarana == 'Y') {
            return redirect('dosen/profile/view')->with('sukses','anda sudah mengirim evaluasi kinerja staf dan prasarana');
        }
        return $next($request);
    }
}
