<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;

use App\Models\admin\staf\HasilEvaluasiStaf_1;
use App\Models\admin\staf\HasilEvaluasiStaf_2;

use Illuminate\Support\Facades\DB;
use PDF;

class HasilEvaluasiStaf extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(HasilEvaluasiStaf_1 $evaluasi){

        $evaluasi = $evaluasi->all();
        return view('admin.staf.hasil.evaluasi_staf',compact('evaluasi'));
    }

    public function view(HasilEvaluasiStaf_2 $view){
        $pertanyaan = DB::table('tbl_pertanyaan_staf')->get();
        $view = \View::make('admin/evaluasi/pdf/hasilevaluasistaf', compact('view', 'pertanyaan'));
        $html_konten = $view->render();

        PDF::SetTitle('Hasil Evaluasi');
        PDF::AddPage();
        PDF::writeHTML($html_konten, true, false, true, false, '');

        PDF::Output($view->noreg . '-' . 'HasilEvaluasi.pdf', 'I');
    }
}
