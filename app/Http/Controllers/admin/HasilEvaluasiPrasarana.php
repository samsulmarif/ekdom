<?php
namespace App\Http\Controllers\admin;

use App\Models\admin\HasilEvaluasiPrasarana2;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use PDF;

class HasilEvaluasiPrasarana extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function hasil_prasarana(){
        $evaluasi = DB::table('tbl_hasil_evaluasi_prasarana')->get();
        return view('admin.prasarana.hasil.evaluasi_prasarana',compact('evaluasi'));
    }

    public function hasil_evaluasi_prasarana(HasilEvaluasiPrasarana2 $view){
        // query all pertanyaan
        $pertanyaan = DB::table('tbl_pertanyaan_prasarana')->get();
        $view = \View::make('admin/evaluasi/pdf/hasilevaluasi', compact('view', 'pertanyaan'));
        $html_konten = $view->render();

        PDF::SetTitle('Hasil Evaluasi');
        PDF::AddPage();
        PDF::writeHTML($html_konten, true, false, true, false, '');

        PDF::Output($view->noreg . '-' . 'HasilEvaluasi.pdf', 'I');
    }
}
