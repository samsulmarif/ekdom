<?php

namespace App\Http\Controllers\admin;
/** models prasarana */
use App\Models\admin\PrasaranaModels;
use App\Models\admin\PertanyaanPrasarana;

/** models staf */
use App\Models\admin\staf\EvaluasiStaf;
use App\Models\admin\staf\PertanyaanStaf;

/** models evaluasi dosen */
use App\Models\admin\EvaluasiDosen;
use App\Models\admin\PertanyaanEvaluasi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Keygen\Keygen;

class EvaluasiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /** function/method dibawah ini untuk evaluasi prasarana */

    public function index_prasarana(PrasaranaModels $prasarana){
        $evaluasi_prasarana = $prasarana->all();
        return view('admin.prasarana.prasarana',compact('evaluasi_prasarana'));
    }

    public function create_prasarana() {
        return view('admin.prasarana.tambahprasarana');
    }

    public function store_prasarana(Request $prasarana){
        $costum = [
            'required' => ':attribute jangan di kosongkan'
        ];
        $prasarana->validate([
            'noreg' => 'required',
            'kategori' => 'required' ,
        ],$costum);

        PrasaranaModels::create([
           'noreg' => $prasarana->noreg,
           'evaluasi' => $prasarana->kategori,
           'nilai' => $prasarana->nilai,
        ]);

        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['prasarana' => 'Y']);

        return redirect('admin/tambah_pertanyaan_prasarana');
    }

    public function edit_prasarana(PrasaranaModels $edit) {
        $evaluasi = $edit;
        return view('admin.prasarana.editpertanyaan',compact('evaluasi'));
    }

    public function update_prasarana(Request $databaru , PrasaranaModels $update) {
        PrasaranaModels::where('id', $update->id)
            ->update([
               'noreg' => $databaru->noreg,
               'evaluasi' => $databaru->kategori,
               'nilai' => $databaru->nilai,
            ]);

        return redirect('admin/prasarana')->with('sukses','data evaluasi prasarana berhasil di ubah');
    }

    // pertanyaan prasarana
    public function index_pertanyaan_prasarana(PertanyaanPrasarana $pertanyaan){
        $pertanyaan = $pertanyaan->all();
        return view('admin.prasarana.pertanyaan',compact('pertanyaan'));
    }

    public function create_pertanyaan_prasarana(PertanyaanPrasarana $pertanyaanPrasarana){
        $pertanyaanPrasarana = $pertanyaanPrasarana->code_pertanyaan();
        return view('admin.prasarana.tambahpertanyaan',compact('pertanyaanPrasarana'));
    }

    public function store_pertanyaan_prasarana(Request $pertanyaan_prasarana) {
        $costum = [
            'required' => ':attribute jangan di kosongkan',
        ];
        $pertanyaan_prasarana->validate([
            'pertanyaan' => 'required',
            'noreg' => 'required',
            'code_pertanyaan' => 'required'
        ],$costum);

        PertanyaanPrasarana::create([
            'pertanyaan' => $pertanyaan_prasarana->pertanyaan,
            'noreg' => $pertanyaan_prasarana->noreg,
            'code_pertanyaan' => $pertanyaan_prasarana->code_pertanyaan,
        ]);

        return redirect('admin/pertanyaan_prasarana')->with('sukses' ,'pertanyaan berhasil di tambahkan');
    }

    public function edit_pertanyaan_prasarana(PertanyaanPrasarana $edit){
        return view('admin.prasarana.editpertanyaanprasarana',compact('edit'));
    }

    public function update_pertanyaan_prasarana(Request $databaru , PertanyaanPrasarana $update){
        $costum = [
            'required' => ':attribute jangan di kosongkan',
        ];
        $databaru->validate([
            'pertanyaan' => 'required',
            'noreg' => 'required',
        ],$costum);

        PertanyaanPrasarana::where('id' , $update->id)
            ->update([
                'pertanyaan' => $databaru->pertanyaan,
                'noreg' => $databaru->noreg,
            ]);
        return redirect('admin/pertanyaan_prasarana')->with('sukses' ,'pertanyaan berhasil di ubah');
    }

    public function delete_pertanyaan_prasarana(PertanyaanPrasarana $delete){
        PertanyaanPrasarana::destroy('id',$delete->id);
        return redirect('admin/pertanyaan_prasarana')->with('sukses' ,'pertanyaan berhasil di hapus');
    }

    /** END:: function/method diatas untuk prasarana */


    /** function dibawah untuk evaluasi kinerja dosen oleh mahasiswa */
    public function index_evaluasi(EvaluasiDosen $evaluasi)
    {
        $evaluasi = $evaluasi->all();
        return view('admin.evaluasi.evaluasi', compact('evaluasi'));
    }

    public function index_pertanyaan(PertanyaanEvaluasi $pertanyaan)
    {
        $pertanyaan = $pertanyaan->all();
        return view('admin.evaluasi.pertanyaan', compact('pertanyaan'));
    }

    public function create_evaluasi()
    {
        return view('admin.evaluasi.tambah_evaluasi');
    }

    public function store_evaluasi(Request $evaluasi)
    {
        $costum = [
            'required' => ':attribute jangan di kosongkan'
        ];
        $evaluasi->validate([
            'noreg' => 'required',
            'kategori' => 'required',
        ], $costum);

        EvaluasiDosen::create([
            'noreg' => $evaluasi->noreg,
            'evaluasi' => $evaluasi->kategori,
            'nilai' => $evaluasi->nilai,
        ]);

        DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(['evaluasi' => 'Y']);


        return redirect('/admin/tambah_pertanyaan');
    }

    public function edit_evaluasi(EvaluasiDosen $evaluasi)
    {
        return view('admin.evaluasi.edit_evaluasi', compact('evaluasi'));
    }

    public function update_evaluasi(Request $databaru, EvaluasiDosen $update)
    {
        $costum = [
            'required' => ':attribute jangan di kosongkan'
        ];
        $databaru->validate([
            'noreg' => 'required',
            'kategori' => 'required',
        ], $costum);
        EvaluasiDosen::where('id', $update->id)
            ->update([
                'noreg' => $databaru->noreg,
                'evaluasi' => $databaru->kategori,
                'nilai' => $databaru->nilai,
            ]);
        return redirect('/admin/evaluasi')->with('sukses', 'data evaluasi berhasil di ubah');
    }

    // Pertanyaan evaluasi kinerja dosen oleh mahasiswa

    public function create_pertanyaan(PertanyaanEvaluasi $pertanyaan)
    {
        $pertanyaan = $pertanyaan->code_pertanyaan();
        return view('admin.evaluasi.tambah_pertanyaan',compact('pertanyaan'));
    }

    public function store_pertanyaan(Request $pertanyaan)
    {
        $costum = [
            'required' => ':attribute jangan di kosongkan'
        ];
        $pertanyaan->validate([
            'pertanyaan' => 'required',
            'noreg' => 'required',
        ], $costum);

        PertanyaanEvaluasi::create([
            'noreg' => $pertanyaan->noreg,
            'pertanyaan' => $pertanyaan->pertanyaan,
            'code_pertanyaan' => $pertanyaan->code_pertanyaan
        ]);

        return redirect('admin/pertanyaan_evaluasi')->with('sukses', 'data pertanyaan berhasil di tambahkan');
    }

    public function edit_pertanyaan(PertanyaanEvaluasi $edit)
    {
        return view('admin.evaluasi.edit_pertanyaan', compact('edit'));
    }

    public function update_pertanyaan(Request $databaru, PertanyaanEvaluasi $update)
    {
        PertanyaanEvaluasi::where('id', $update->id)
            ->update([
                'noreg' => $databaru->noreg,
                'pertanyaan' => $databaru->pertanyaan
            ]);

        return redirect('/admin/pertanyaan_evaluasi')->with('sukses', 'data berhasl di update');
    }

    public function delete_pertanyaan(PertanyaanEvaluasi $delete)
    {
        PertanyaanEvaluasi::destroy('id', $delete->id);
        return redirect('/admin/pertanyaan_evaluasi')->with('sukses', 'data berhasl di hapus');
    }

    /** END:: function diatas untuk evaluasi kinerja dosen oleh mahasiswa */


    /** function dibawah untuk evaluasi staf */

    public function index_staf(EvaluasiStaf $evaluasi_staf) {
        $evaluasi_staf = $evaluasi_staf->all();
        return view('admin.staf.staf',compact('evaluasi_staf'));
    }

    public function create_evaluasi_staf(){
        return view('admin.staf.tambah_staf');
    }

    public function store_evaluasi_staf(Request $staf){

        $costum = [
            'required' => ':attribute jangan di kosongkan',
        ];
        $staf->validate([
            'noreg' => 'required',
            'kategori' => 'required',
        ],$costum);

        EvaluasiStaf::create([
            'noreg' => $staf->noreg,
            'nilai' => $staf->nilai,
            'evaluasi' => $staf->kategori,
        ]);

        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['staf' => 'Y']);

        return redirect('/admin/tambah_pertanyaan_staf');

    }

    public function edit_evaluasi_staf(EvaluasiStaf $edit)
    {
        return view('admin.staf.edit_staf',compact('edit'));
    }

    public function update_evaluasi_staf(Request $databaru , EvaluasiStaf $update)
    {
        $costum = [
            'required' => ':attribute jangan dikosongkan',
        ];
        $databaru->validate([
            'noreg' => 'required',
            'kategori' => 'required'
        ],$costum);
        EvaluasiStaf::where('id', $update->id)
            ->update([
               'noreg' => $databaru->noreg,
               'nilai' => $databaru->nilai,
               'evaluasi' => $databaru->kategori,
            ]);
        return redirect('admin/staf')->with('sukses','data berhasil di ubah');
    }

    // Pertanyaan staf

    public function index_pertanyaan_staf(PertanyaanStaf $pertanyaan){
        $pertanyaan = $pertanyaan->all();
        return view('admin.staf.pertanyaan',compact('pertanyaan'));
    }

    public function create_pertanyaan_staf(PertanyaanStaf $pertanyaanStaf){
        $pertanyaanStaf = $pertanyaanStaf->code_pertanyaan();
        return view('admin.staf.tambah_pertanyaan_staf',compact('pertanyaanStaf'));
    }

    public function store_pertanyaan_staf(Request $stafpertanyaan){
        $costum = [
            'required' => ':attribute jangan dikosongkan',
        ];
        $stafpertanyaan->validate([
           'noreg' => 'required',
           'pertanyaan' => 'required',
           'code_pertanyaan' => 'required',
        ],$costum);

        PertanyaanStaf::create([
           'noreg' => $stafpertanyaan->noreg,
           'pertanyaan' => $stafpertanyaan->pertanyaan,
           'code_pertanyaan' => $stafpertanyaan->code_pertanyaan,
        ]);

        return redirect('admin/pertanyaan_staf')->with('sukses','data pertanyaan evaluasi staf berhasil di tambahkan');

    }

    public function edit_pertanyaan_staf(PertanyaanStaf $edit)
    {
        return view('admin.staf.edit_pertanyaan_staf',compact('edit'));
    }

    public function update_pertanyaan_staf(Request $databaru , PertanyaanStaf $update){

        $costum = [
            'required' => ':attribute jangan di kosongkan',
        ];
        $databaru->validate([
            'noreg' => 'required',
            'pertanyaan' => 'required',
        ],$costum);

        PertanyaanStaf::where('id',$update->id)
            ->update([
                'noreg' => $databaru->noreg,
                'pertanyaan' => $databaru->pertanyaan,
            ]);

        return redirect('admin/pertanyaan_staf')->with('sukses','data evaluasi staf berhasil di ubah');

    }

    public function delete_pertanyaan_staf(PertanyaanStaf $delete){
        PertanyaanStaf::destroy('id',$delete->id);
        return redirect('admin/pertanyaan_staf')->with('sukses','data evaluasi staf berhasil di hapus');
    }




}
