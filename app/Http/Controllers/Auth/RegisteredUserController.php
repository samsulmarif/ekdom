<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Keygen\Keygen;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('errors.505');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $pesan_validasi = [
            'required' => ':attribute jangan di kosongkan',
            'email' => 'bukan penulisan email ',
            'max' => 'nama dan email maksimal 255 karakter ',
            'min' => 'nama dan email minimal 8 karakter ',
            'unique' => 'email yang di masukan sudah pernah ada sebelumnya',
            'same' => 'password yang anda ketikan tidak sama'
        ];
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'conf_password' => 'required|same:password|min:8'
        ], $pesan_validasi);

        Auth::login($user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'noreg' => Keygen::numeric(12)->prefix('EKDOM-')->generate(),
            'password' => Hash::make($request->password),
            'role' => 'mahasiswa'
        ]));

        event(new Registered($user));

        return redirect(RouteServiceProvider::HOME);
    }
}
