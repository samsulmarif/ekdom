<?php
namespace App\Http\Controllers\depan;

use App\Models\depan\DepanModels;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardDepan extends Controller{

    public function index(DepanModels $users){
        $data = array(
          'evaluasi' =>  DB::table('tbl_hasil_evaluasi_2')->count(),
          'mahasiswa' => DB::table('tbl_mahasiswa')->count(),
          'dosen' => DB::table('tbl_dosen')->count(),
        );

        return view('depan.DepanDashboard',compact('data'));
    }
}
