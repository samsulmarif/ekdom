<?php

namespace App\Http\Controllers\dosen;

use App\Models\dosen\PertanyaanModels;
use App\Models\dosen\EvaluasiModels;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanEvaluasi extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(PertanyaanModels $pertanyaan)
    {
        $pertanyaan = $pertanyaan->all();
        return view('dosen.evaluasi.pertanyaan', compact('pertanyaan'));
    }

    public function index_evaluasi(EvaluasiModels $evaluasi)
    {
        $evaluasi = $evaluasi->all();
        return view('dosen.evaluasi.evaluasi', compact('evaluasi'));
    }

    public function create_evaluasi()
    {
        return view('dosen.evaluasi.kategori_evaluasi');
    }

    public function store_evaluasi(Request $evaluasi)
    {
        $costum_validate = [
            'required' => ':attribute jangan di kosongkan',
        ];
        $evaluasi->validate([
            'noreg' => 'required',
            'dosen' => 'required',
            'kategori' => 'required',
        ], $costum_validate);
        EvaluasiModels::create([
            'noreg' => $evaluasi->noreg,
            'dosen' => $evaluasi->dosen,
            'evaluasi' => $evaluasi->kategori,
            'nilai' => $evaluasi->nilai,
        ]);

        DB::table('users')
            ->where('id', $evaluasi->id)
            ->update(['evaluasi' => 'Y']);

        return redirect('/dosen/tambah_pertanyaan');
    }

    public function create()
    {
        return view('dosen.evaluasi.tambah_pertanyaan');
    }

    public function store(Request $pertanyaan)
    {
        $costum_validate = [
            'required' => ':attribute jangan di kosongkan',
        ];
        $pertanyaan->validate([
            'noreg' => 'required',
            'pertanyaan' => 'required',
        ], $costum_validate);
        PertanyaanModels::create([
            'noreg' => $pertanyaan->noreg,
            'pertanyaan' => $pertanyaan->pertanyaan,
        ]);

        return redirect('/dosen/pertanyaan_evaluasi')->with('sukses', 'Data Pertanyaan Di Tambahkan');
    }

    public function edit(PertanyaanModels $edit)
    {
        return view('dosen.evaluasi.edit_pertanyaan', compact('edit'));
    }

    public function update(Request $databaru, PertanyaanModels $update)
    {
        PertanyaanModels::where('id', $update->id)
            ->update([
                'pertanyaan' => $databaru->pertanyaan,
                'noreg' => $databaru->noreg,
                'nilai' => $databaru->nilai,
            ]);

        return redirect('/dosen/pertanyaan_evaluasi')->with('sukses', 'pertanyaan berhasil di ubah');
    }

    public function delete(PertanyaanModels $delete)
    {
        PertanyaanModels::destroy('id', $delete->id);
        return redirect('/dosen/pertanyaan_evaluasi')->with('sukses', 'pertanyaan berhasil di Hapus');
    }
}
