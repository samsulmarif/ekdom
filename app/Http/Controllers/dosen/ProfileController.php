<?php

namespace App\Http\Controllers\dosen;

use App\Http\Controllers\Controller;
use App\Models\dosen\DosenModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function reset_pass(DosenModels $getdosenpass)
    {
        $datapass = $getdosenpass->all();
        foreach ($datapass as $d) {
            if (Auth::user()->noreg == $d->noreg) {
                $pass_dosen = $d;
            }
        }

        return view('dosen.formspass', compact('pass_dosen'));
    }

    public function update_pass(Request $passbaru, DosenModels $passupdate)
    {
        $costum_validasi = [
            'required' => ':attribute jangan di kosongkan',
            'same' => 'password yang anda ketikan tidak sama'
        ];
        $passbaru->validate([
            'password' => 'required|max:128',
            'conf_pass' => 'required|same:password',
        ], $costum_validasi);

        DosenModels::where('id', $passupdate->id)
            ->update([
                'password' => Hash::make($passbaru->password)
            ]);
        return redirect('dosen/profile/view')->with('sukses', 'password berhasil di ubah');
    }

    public function profile_dosen(DosenModels $getprofiledosen)
    {
        $getprofile = $getprofiledosen->all();
        return view('dosen.profile', compact('getprofile'));
    }

    public function get_data_dosen(DosenModels $edit)
    {
        return view('dosen.update_profile_dosen', compact('edit'));
    }

    public function update_data_profile(Request $profilebaru, DosenModels $update)
    {
        $costum_validasi = [
            'required' => ':attribute jangan di kosongkan',
            'same' => 'password yang anda ketikan tidak sama'
        ];
        $profilebaru->validate([
            'email' => 'required',
            'nama' => 'required',
            'hobi' => 'required',
            'alamat' => 'required',
            'nip' => 'required',
            'jurusan' => 'required',
            'no_wa' => 'required',
            'tentang_data_diri' => 'required',
        ], $costum_validasi);

        DosenModels::where('id', $update->id)
            ->update([
                'email' => $profilebaru->email,
                'name' => $profilebaru->nama,
                'hobi' => $profilebaru->hobi,
                'Nim_Nidn_Nip' => $profilebaru->nip,
                'jurusan' => $profilebaru->jurusan,
                'alamat' => $profilebaru->alamat,
                'no_wa' => $profilebaru->no_wa,
                'about' => $profilebaru->tentang_data_diri
            ]);

        DB::table('users')
            ->where('id', $update->id)
            ->update(['biodata' => 'Y']);

        return redirect('dosen/profile/view')->with('sukses', 'data profile anda berhasil di ubah');
    }
}
