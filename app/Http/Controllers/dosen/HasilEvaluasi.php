<?php

namespace App\Http\Controllers\dosen;

use App\Models\dosen\HasilEvaluasiModels;
use App\Models\dosen\HasilPertanyaanModels;

//models get pertanyaan
use App\Models\dosen\KumpulanPertanyaan;
use App\Http\Controllers\Controller;

// TCPDF
use PDF;

class HasilEvaluasi extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function hasil_evaluasi_mahasiswa(HasilEvaluasiModels $evaluasi)
    {
        $evaluasi = $evaluasi->all();
        return view('dosen.evaluasi.hasil.hasilevaluasi', compact('evaluasi'));
    }

    public function lihat_hasil_evaluasi(HasilPertanyaanModels $view, KumpulanPertanyaan $pertanyaan)
    {
        // query all pertanyaan
        $pertanyaan = $pertanyaan->all();
        $view = \View::make('dosen/evaluasi/pdf/hasilevaluasi', compact('view', 'pertanyaan'));
        $html_konten = $view->render();

        PDF::SetTitle('Hasil Evaluasi Prasarana Fakultas');
        PDF::AddPage();
        PDF::writeHTML($html_konten, true, false, true, false, '');

        PDF::Output($view->noreg . '-' . 'HasilEvaluasi.pdf', 'I');
    }
}
