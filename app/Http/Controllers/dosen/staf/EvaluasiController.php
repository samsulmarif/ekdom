<?php
namespace App\Http\Controllers\dosen\staf;

/** insert hasil evaluasi staf */
use App\Models\dosen\EvaluasiStaf;
use App\Models\dosen\HasilEvaluasiStaf;

/** models submit evaluasi prasarana */
use App\Models\mahasiswa\KirimEvaluasiPrasarana;
use App\Models\mahasiswa\KirimPertanyaanPrasarana;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EvaluasiController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $evaluasi = DB::table('tbl_evaluasi_staf')->get();
        return view('dosen.evaluasi.staf.view_evaluasi', compact('evaluasi'));
    }

    public function evaluasi_staf()
    {
        $pertanyaan = DB::table('tbl_pertanyaan_staf')->get();
        $evaluasi = DB::table('tbl_evaluasi_staf')->get();
        return view('dosen.evaluasi.staf.evaluasi_staf', compact('pertanyaan', 'evaluasi'));
    }

    public function insert_evaluasi_staf(Request $evaluasi_staf)
    {
        $costum = [
            'required' => ':attribute jangan dikosongkan'
        ];
        $evaluasi_staf->validate([
            'noreg_mahasiswa' => 'required',
            'saran' => 'required',
            'kategori' => 'required',
        ], $costum);

        HasilEvaluasiStaf::create([
            'noreg' => $evaluasi_staf->noreg_mahasiswa,
            'saran' => $evaluasi_staf->saran,
            'kategori' => $evaluasi_staf->kategori,
            'nim' => $evaluasi_staf->nip,
            'jurusan' => $evaluasi_staf->jurusan,
        ]);

        EvaluasiStaf::create([
            'hasil' => $evaluasi_staf->ekdom0001,
            'hasil_2' => $evaluasi_staf->ekdom0002,
            'hasil_3' => $evaluasi_staf->ekdom0003,
            'hasil_4' => $evaluasi_staf->ekdom0004,
            'hasil_5' => $evaluasi_staf->ekdom0005,
            'hasil_6' => $evaluasi_staf->ekdom0006,
            'hasil_7' => $evaluasi_staf->ekdom0007,
            'hasil_8' => $evaluasi_staf->ekdom0008,
            'hasil_9' => $evaluasi_staf->ekdom0009,
            'hasil_10' => $evaluasi_staf->ekdom0010,
            'noreg' => $evaluasi_staf->noreg_mahasiswa,
            'nim' => $evaluasi_staf->nip,
            'jurusan' => $evaluasi_staf->jurusan,
            'nama_evaluasi' => $evaluasi_staf->nama_mahasiswa
        ]);

        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['staf' => 'Y']);

        return redirect('dosen/kirim_evaluasi_prasarana');
    }

    public function evaluasi_prasarana()
    {
        $pertanyaan = DB::table('tbl_pertanyaan_prasarana')->get();
        $evaluasi = DB::table('tbl_evaluasi_prasarana')->get();
        return view('dosen.evaluasi.prasarana.evaluasi_prasarana', compact('pertanyaan', 'evaluasi'));
    }

    public function insert_evaluasi_prasarana(Request $prasarana)
    {
        $costum = [
            'required' => ':attribute jangan dikosongkan'
        ];
        $prasarana->validate([
            'noreg_mahasiswa' => 'required',
            'saran' => 'required',
            'kategori' => 'required',
        ], $costum);

        KirimEvaluasiPrasarana::create([
            'noreg' => $prasarana->noreg_mahasiswa,
            'saran' => $prasarana->saran,
            'kategori' => $prasarana->kategori,
            'nim' => $prasarana->nip,
            'jurusan' => $prasarana->jurusan,
        ]);

        KirimPertanyaanPrasarana::create([
            'hasil' => $prasarana->ekdom0001,
            'hasil_2' => $prasarana->ekdom0002,
            'hasil_3' => $prasarana->ekdom0003,
            'hasil_4' => $prasarana->ekdom0004,
            'hasil_5' => $prasarana->ekdom0005,
            'hasil_6' => $prasarana->ekdom0006,
            'hasil_7' => $prasarana->ekdom0007,
            'hasil_8' => $prasarana->ekdom0008,
            'hasil_9' => $prasarana->ekdom0009,
            'hasil_10' => $prasarana->ekdom0010,
            'noreg' => $prasarana->noreg_mahasiswa,
            'nim' => $prasarana->nip,
            'jurusan' => $prasarana->jurusan,
            'nama_evaluasi' => $prasarana->nama_mahasiswa
        ]);

        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['prasarana' => 'Y']);

        return redirect('dosen/profile/view')->with('sukses','evaluasi staf dan prasarana berhasil dikirim terimakasih');

    }

}
