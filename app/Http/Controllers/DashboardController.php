<?php

namespace App\Http\Controllers;
use App\Models\ModelsUsers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function DashboardAdmin()
    {
        $mahasiswa = DB::table('tbl_mahasiswa')->count();
        $dosen = DB::table('tbl_dosen')->count();
        $evaluasi_kinerja_staf = DB::table('tbl_hasil_evaluasi_staf')->count();
        $prasarana = DB::table('tbl_hasil_evaluasi_prasarana')->count();
        return view('admin.dashboard',compact('dosen','mahasiswa','prasarana','evaluasi_kinerja_staf'));
    }

    public function DashboardMahasiswa()
    {
        return view('mahasiswa.mahasiswa');
    }

    public function DashboardDosen(ModelsUsers  $evaluasi)
    {
        $evaluasi = $evaluasi->all();
        $data_dosen = DB::table('tbl_dosen')->count();
        return view('dosen.dosen',compact('evaluasi','data_dosen'));
    }
}
