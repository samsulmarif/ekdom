<?php

namespace App\Http\Controllers\mahasiswa;

use App\Models\ModelsUsers;

use App\Models\mahasiswa\PasswordModels;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function resetPassword(PasswordModels $getdatapass)
    {
        $pass = $getdatapass->all();
        foreach ($pass as $p) {
            if (Auth::user()->noreg == $p->noreg) {
                $id_pass = $p;
            }
        }
        return view('mahasiswa.resetpass', compact('id_pass'));
    }

    public function ubahPassword(Request $passbaru, PasswordModels $pass)
    {
        $costum_validasi = [
            'required' => ':attribute jangan di kosongkan',
            'same' => 'password yang anda ketikan tidak sama'
        ];
        $passbaru->validate([
            'password' => 'required|max:128',
            'conf_pass' => 'required|same:password',
        ], $costum_validasi);

        PasswordModels::where('id', $pass->id)
            ->update([
                'password' => Hash::make($passbaru->password)
            ]);
        return redirect('mahasiswa/profile/view')->with('sukses', 'password berhasil di ubah');
    }


    public function profile(ModelsUsers $getprofile)
    {
        $getprofile = $getprofile->all();
        return view('mahasiswa.profile', compact('getprofile'));
    }

    public function edit_profile(ModelsUsers $edit)
    {
        return view('mahasiswa.updateprofile', compact('edit'));
    }

    public function update_profile(Request $databaru, ModelsUsers $update)
    {
        $costum_validasi = [
            'required' => ':attribute jangan di kosongkan',
            'same' => 'password yang anda ketikan tidak sama'
        ];
        $databaru->validate([
            'email' => 'required',
            'nama' => 'required',
            'hobi' => 'required',
            'alamat' => 'required',
            'jurusan' => 'required',
            'nim' => 'required',
            'no_wa' => 'required',
            'tentang_data_diri' => 'required',
        ], $costum_validasi);

        ModelsUsers::where('id', $update->id)
            ->update([
                'email' => $databaru->email,
                'name' => $databaru->nama,
                'hobi' => $databaru->hobi,
                'Nim_Nidn_Nip' => $databaru->nim,
                'jurusan' => $databaru->jurusan,
                'alamat' => $databaru->alamat,
                'no_wa' => $databaru->no_wa,
                'about' => $databaru->tentang_data_diri
            ]);
        DB::table('users')
            ->where('id', $update->id)
            ->update(['biodata' => 'Y']);

        return redirect('mahasiswa/profile/view')->with('sukses', 'data profile anda berhasil di ubah');
    }
}
