<?php

namespace App\Http\Controllers\mahasiswa;

/** models evaluasi kinerja dosen */
use App\Models\mahasiswa\EvaluasiModels;
use App\Models\mahasiswa\PertanyaanModels;
/** models evaluasi prasarana */
use App\Models\mahasiswa\PrasaranaModels;
use App\Models\mahasiswa\PertanyaanPrasarana;

/** hasil evaluasi models */
use App\Models\mahasiswa\HasilEvaluasiModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/** models submit evaluasi kinerja dosen dan pertanyaan */
use App\Models\mahasiswa\KirimEvaluasi;
use App\Models\mahasiswa\KirimPertanyaan;

/** models submit evaluasi prasarana dan pertanyaan */
use App\Models\mahasiswa\KirimEvaluasiPrasarana;
use App\Models\mahasiswa\KirimPertanyaanPrasarana;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EvaluasiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(EvaluasiModels $evaluasi, PertanyaanModels $pertanyaan)
    {
        $evaluasi = $evaluasi->all();
        return view('mahasiswa.evaluasi.view_evaluasi', compact('evaluasi'));
    }

    public function evaluasi(PertanyaanModels $pertanyaan, EvaluasiModels $evaluasi)
    {
        $pertanyaan = $pertanyaan->all();
        $evaluasi = $evaluasi->all();
        return view('mahasiswa.evaluasi.evaluasi', compact('pertanyaan', 'evaluasi'));
    }

    public function prasarana(PrasaranaModels $prasarana , PertanyaanPrasarana $pertanyaan) {
        $pertanyaan = $pertanyaan->all();
        $evaluasi = $prasarana->all();
        return view('mahasiswa.evaluasi.prasarana.evaluasi',compact('pertanyaan','evaluasi'));
    }


    public function insert_evaluasi(Request $insert_evaluasi)
    {
        $costum = [
            'required' => ':attribute jangan dikosongkan'
        ];
        $insert_evaluasi->validate([
            'noreg_mahasiswa' => 'required',
            'saran' => 'required',
            'kategori' => 'required',
        ], $costum);

        KirimEvaluasi::create([
            'noreg' => $insert_evaluasi->noreg_mahasiswa,
            'saran' => $insert_evaluasi->saran,
            'kategori' => $insert_evaluasi->kategori,
            'nim' => $insert_evaluasi->nim,
            'jurusan' => $insert_evaluasi->jurusan,
        ]);

        KirimPertanyaan::create([
            'hasil' => $insert_evaluasi->ekdom0001,
            'hasil_2' => $insert_evaluasi->ekdom0002,
            'hasil_3' => $insert_evaluasi->ekdom0003,
            'hasil_4' => $insert_evaluasi->ekdom0004,
            'hasil_5' => $insert_evaluasi->ekdom0005,
            'hasil_6' => $insert_evaluasi->ekdom0006,
            'hasil_7' => $insert_evaluasi->ekdom0007,
            'hasil_8' => $insert_evaluasi->ekdom0008,
            'hasil_9' => $insert_evaluasi->ekdom0009,
            'hasil_10' => $insert_evaluasi->ekdom0010,
            'noreg' => $insert_evaluasi->noreg_mahasiswa,
            'nim' => $insert_evaluasi->nim,
            'jurusan' => $insert_evaluasi->jurusan,
            'nama_evaluasi' => $insert_evaluasi->nama_mahasiswa
        ]);

        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['evaluasi' => 'Y']);

        return redirect('mahasiswa/kirim_evaluasi_prasarana');

    }

    public function insert_evaluasi_prasarana(Request $prasarana){
        $costum = [
            'required' => ':attribute jangan dikosongkan'
        ];
        $prasarana->validate([
            'noreg_mahasiswa' => 'required',
            'saran' => 'required',
            'kategori' => 'required',
        ], $costum);

        KirimEvaluasiPrasarana::create([
            'noreg' => $prasarana->noreg_mahasiswa,
            'saran' => $prasarana->saran,
            'kategori' => $prasarana->kategori,
            'nim' => $prasarana->nim,
            'jurusan' => $prasarana->jurusan,
        ]);

        KirimPertanyaanPrasarana::create([
            'hasil' => $prasarana->ekdom0001,
            'hasil_2' => $prasarana->ekdom0002,
            'hasil_3' => $prasarana->ekdom0003,
            'hasil_4' => $prasarana->ekdom0004,
            'hasil_5' => $prasarana->ekdom0005,
            'hasil_6' => $prasarana->ekdom0006,
            'hasil_7' => $prasarana->ekdom0007,
            'hasil_8' => $prasarana->ekdom0008,
            'hasil_9' => $prasarana->ekdom0009,
            'hasil_10' => $prasarana->ekdom0010,
            'noreg' => $prasarana->noreg_mahasiswa,
            'nim' => $prasarana->nim,
            'jurusan' => $prasarana->jurusan,
            'nama_evaluasi' => $prasarana->nama_mahasiswa
        ]);

        DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(['prasarana' => 'Y']);

        return redirect('mahasiswa/profile/view')->with('sukses','evaluasi kinerja dosen dan prasarana sukses');

    }

    public function hasil_evaluasi(HasilEvaluasiModel $hasilevaluasi) {
        $hasilevaluasi = $hasilevaluasi->all();
        return view('mahasiswa.hasil.evaluasi',compact('hasilevaluasi'));
    }

}
