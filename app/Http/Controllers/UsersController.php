<?php

namespace App\Http\Controllers;

use App\Models\ModelsUsers;
use App\Models\admin\ModelsTblDosen;
use App\Models\admin\ModelsTblMahasiswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Keygen\Keygen;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('users')->get();
        return view('admin.users.usersview', compact('data'));
    }

    public function mahasiswa(ModelsUsers $getMahasiswa)
    {
        $getMahasiswa = $getMahasiswa->all();
        return view('admin.users.viewmahasiswa', compact('getMahasiswa'));
    }

    public function dosen(ModelsUsers $getDosen)
    {
        $getDosen = $getDosen->all();
        return view('admin.users.viewdosen', compact('getDosen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.userstambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $costum_validate = [
            'min' => 'minimal 4 karakter',
            'max' => 'maksimal 100 karakter',
            'required' => ':attribute jangan di kosongkan',
            'unique' => 'email yang anda gunakan sdh ada harap masukan email lain',
            'same' => 'password anda ketikan tidak sama',
            'email' => 'penulisan email tidak sesuai'
        ];
        $request->validate([
            'nama' => 'required|max:100|min:4',
            'email' => 'required|email|unique:users|email',
            'password' => 'required',
            'conf-password' => 'required|same:password',
        ], $costum_validate);
        ModelsUsers::create([
            'name' => $request->nama,
            'email' => $request->email,
            'noreg' => Keygen::numeric(12)->prefix('EKDOM-')->generate(),
            'password' => Hash::make($request->password),
            'role' => $request->role_users,
        ]);

        if($request->role_users == 'mahasiswa') {
            ModelsTblMahasiswa::create([
                'name' => $request->nama,
                'role' => $request->role_users,
                'noreg' => Keygen::numeric(12)->prefix('EKDOM-')->generate(),
            ]);
        } else if($request->role_users == 'dosen') {
            ModelsTblDosen::create([
                'name' => $request->nama,
                'role' => $request->role_users,
                'noreg' => Keygen::numeric(12)->prefix('EKDOM-')->generate(),
            ]);
        }

        return redirect('/admin/users')->with('sukses', 'Data Berhasil Di Tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ModelsUsers  $modelsUsers
     * @return \Illuminate\Http\Response
     */
    public function show(ModelsUsers $status_mahasiswa)
    {
        // lacak status mahasiswa
        return view('admin.lacak.status_mahasiswa',compact('status_mahasiswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ModelsUsers  $modelsUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelsUsers $modelsUsers)
    {
        return view('admin.users.usersedit', compact('modelsUsers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ModelsUsers  $modelsUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModelsUsers $modelsUsers)
    {
        $costum_validate = [
            'min' => 'minimal 4 karakter',
            'max' => 'maksimal 100 karakter',
            'required' => ':attribute jangan di kosongkan',
            'same' => 'password anda ketikan tidak sama'
        ];
        $request->validate([
            'nama' => 'required|max:100|min:4',
            'email' => 'required|email',
            'password' => 'required',
            'conf-password' => 'required|same:password',
        ], $costum_validate);

        ModelsUsers::where('id', $modelsUsers->id)
            ->update([
                'name' => $request->nama,
                'email' => $request->email,
                'noreg' => $request->noreg,
                'password' => Hash::make($request->password)
            ]);
        return redirect('/admin/users')->with('sukses', 'Data Berhasil Di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ModelsUsers  $modelsUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModelsUsers $modelsUsers)
    {
        ModelsUsers::destroy($modelsUsers->id);
        return redirect('/admin/users')->with('sukses', 'Data Berhasil Di Hapus');
    }
}
