<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PertanyaanModels extends Model
{
    protected $table = 'tbl_pertanyaan';
    use HasFactory;
}
