<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KirimPertanyaan extends Model
{
    protected $table = 'tbl_hasil_evaluasi_2';
    protected $fillable = ['hasil', 'noreg','nama_evaluasi','hasil_2','hasil_3','hasil_4','hasil_5','hasil_6','hasil_7','hasil_8','hasil_9','hasil_10','nim','jurusan'];
    use HasFactory;
}
