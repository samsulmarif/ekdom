<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilEvaluasiModel extends Model
{
    protected $table = 'users';
    use HasFactory;
}
