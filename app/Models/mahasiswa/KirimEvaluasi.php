<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KirimEvaluasi extends Model
{
    protected $table = 'tbl_hasil_evaluasi';
    protected $fillable = ['noreg', 'saran', 'kategori','nim','jurusan'];
    use HasFactory;
}
