<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasswordModels extends Model
{
    protected $table = 'users';
    protected $fillable = ['password'];
    use HasFactory;
}
