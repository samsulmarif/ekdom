<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EvaluasiModels extends Model
{
    protected $table = 'tbl_evaluasi';
    use HasFactory;
}
