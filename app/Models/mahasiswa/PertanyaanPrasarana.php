<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PertanyaanPrasarana extends Model
{
    protected $table = 'tbl_pertanyaan_prasarana';
    use HasFactory;
}
