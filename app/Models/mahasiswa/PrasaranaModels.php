<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrasaranaModels extends Model
{
    protected $table = 'tbl_evaluasi_prasarana';
    use HasFactory;
}
