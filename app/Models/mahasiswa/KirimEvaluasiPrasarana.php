<?php

namespace App\Models\mahasiswa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KirimEvaluasiPrasarana extends Model
{
    protected $table = 'tbl_hasil_evaluasi_prasarana';

    protected $fillable = ['noreg','kategori','saran','nim','jurusan'];
    use HasFactory;
}
