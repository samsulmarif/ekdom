<?php

namespace App\Models\depan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DepanModels extends Model
{
    protected $table = 'users';
    use HasFactory;
}
