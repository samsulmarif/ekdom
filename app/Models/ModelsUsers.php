<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelsUsers extends Model
{
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'role', 'noreg', 'about', 'no_wa', 'alamat', 'hobi', 'foto_profile', 'Nim_Nidn_Nip','jurusan'];
    use HasFactory;
}
