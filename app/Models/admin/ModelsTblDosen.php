<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelsTblDosen extends Model
{
    protected $table = 'tbl_dosen';
    protected $fillable = ['name','noreg','role'];
    use HasFactory;
}
