<?php

namespace App\Models\admin\staf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilEvaluasiStaf_1 extends Model
{
    protected $table = 'tbl_hasil_evaluasi_staf';
    protected $fillable = ['noreg','kategori','saran','nim','jurusan'];
    use HasFactory;
}
