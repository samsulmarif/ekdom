<?php

namespace App\Models\admin\staf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EvaluasiStaf extends Model
{
    protected $table = 'tbl_evaluasi_staf';
    protected $fillable = ['noreg','evaluasi','nilai'];
    use HasFactory;
}
