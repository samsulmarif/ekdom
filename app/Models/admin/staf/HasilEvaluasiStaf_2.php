<?php

namespace App\Models\admin\staf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilEvaluasiStaf_2 extends Model
{
    protected $table = 'tbl_hasil_evaluasi_staf_2';
    protected $fillable = ['hasil', 'noreg','nim','jurusan','nama_evaluasi','hasil_2','hasil_3','hasil_4','hasil_5','hasil_6','hasil_7','hasil_8','hasil_9','hasil_10'];
    use HasFactory;
}
