<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelsTblMahasiswa extends Model
{
    protected $table = 'tbl_mahasiswa';
    protected $fillable = ['name','noreg','role'];
    use HasFactory;
}
