<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EvaluasiDosen extends Model
{
    protected $table = 'tbl_evaluasi';
    protected $fillable = ['noreg', 'dosen', 'evaluasi', 'nilai'];
    use HasFactory;
}
