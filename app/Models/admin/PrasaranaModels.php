<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrasaranaModels extends Model
{
    protected $table = 'tbl_evaluasi_prasarana';
    protected $fillable = ['noreg','evaluasi','nilai'];
    use HasFactory;
}
