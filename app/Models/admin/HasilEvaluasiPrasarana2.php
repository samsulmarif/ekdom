<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilEvaluasiPrasarana2 extends Model
{
    protected $table = 'tbl_hasil_evaluasi_prasarana_2';
    use HasFactory;
}
