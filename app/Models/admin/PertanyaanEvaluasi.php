<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PertanyaanEvaluasi extends Model
{
    protected $table = 'tbl_pertanyaan';
    protected $fillable = ['pertanyaan', 'noreg','code_pertanyaan'];

    public static function code_pertanyaan(){
        $code = DB::table('tbl_pertanyaan')->max('code_pertanyaan');
        $spasi = '';
        $code = str_replace("ekdom","",$code);
        $code = (int) $code + 1;
        $inc_code = $code;

        if(strlen($code) == 1) {
            $spasi = "000";
        }else if(strlen($code) == '2'){
            $spasi = "00";
        } else if(strlen($code == 3)) {
            $spasi = "0";
        }

        $create_code = "ekdom". $spasi.$inc_code;
        return $create_code;
    }
    use HasFactory;
}
