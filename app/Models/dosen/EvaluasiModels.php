<?php

namespace App\Models\dosen;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EvaluasiModels extends Model
{
    protected $table = 'tbl_evaluasi';
    protected $fillable = ['noreg', 'dosen', 'evaluasi', 'nilai'];
    use HasFactory;
}
