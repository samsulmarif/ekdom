<?php

namespace App\Models\dosen;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilEvaluasiStaf extends Model
{
    protected $table = 'tbl_hasil_evaluasi_staf';
    protected $fillable = ['noreg','kategori','nim','jurusan','saran'];
    use HasFactory;
}
