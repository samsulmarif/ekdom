<?php

namespace App\Models\dosen;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilEvaluasiModels extends Model
{
    protected $table = 'tbl_hasil_evaluasi';
    use HasFactory;
}
