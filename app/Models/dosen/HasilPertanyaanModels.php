<?php

namespace App\Models\dosen;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HasilPertanyaanModels extends Model
{
    protected $table = 'tbl_hasil_evaluasi_2';
    use HasFactory;
}
