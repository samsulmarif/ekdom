<?php

namespace App\Models\dosen;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PertanyaanModels extends Model
{
    protected $table = 'tbl_pertanyaan';
    protected $fillable = ['pertanyaan', 'noreg'];
    use HasFactory;
}
