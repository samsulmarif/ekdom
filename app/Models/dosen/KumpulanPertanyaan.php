<?php

namespace App\Models\dosen;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KumpulanPertanyaan extends Model
{
    protected $table = 'tbl_pertanyaan';
    use HasFactory;
}
