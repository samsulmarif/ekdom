<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        table {
            font-family: 'Times New Roman', Times, serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #000000;
            text-align: center;
            height: 20px;
            margin: 8px;
        }

        p {
            font-family: 'Times New Roman', Times, serif;
        }

        h4 {
            font-family: 'Times New Roman', Times, serif;
        }

        .line_title {
            border: 0;
            border-style: inset;
            border-top: 1px solid #000;
        }
    </style>
</head>


<body>
<img src="{{ asset('assets/img/uho.png') }}" style="position:absolute; width: 70px; height:auto;">
<!-- <center><b>UNIVERSITAS HALUOLEO <br> SULAWESI TENGGARA</b></center> -->
<h4 align="center">UNIVERSITAS HALUOLEO <br> SULAWESI TENGGARA</h4>

<hr class="line-title">

<p style="width: 100%;" align="center"><b>EVALUASI PRASARANA FAKULTAS FIB UHO</b></p>

<p>Tujuan dari kuesioner ini adalah untuk meningkatkan kualitas belajar mengajar pada institusi kita. Saran dan masukkan dari mahasiswa merupakan input yang sangat berharga. Untuk itu kami sangat berharap agar kuesioner ini diisi dengan sungguh-sungguh</p>
<p>
    Noreg : {{ $view->noreg }} <br>
    Submit : {{ $view->created_at }}<br>
    Nama : {{ $view->nama_evaluasi }}<br>
    Nim/NIP/NIDN : {{ $view->nim }}<br>
    Jurusan : {{ $view->jurusan }}<br>
</p>
<p>Berikut hasil penilaian mahasiswa terhadap prasarana fakultas:</p>
<table cellpadding="6" class="table table-dark">
    <thead>
    <tr>
        <th class="text-center" ><b>No</b></th>
        <th class="text-center" colspan="5"><b>Materi
                Penilaian</b></th>
        <th class="text-center" colspan="5"><b>Hasil Penilaian
            </b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($pertanyaan as $p)
        <tr>
            <td class="text-center">{{ $loop->iteration }}</td>
            <td class="text-center" colspan="5">{{ $p->pertanyaan }}</td>
            @if($p->id == 1)
                @if($view->hasil == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 2)
                @if($view->hasil_2 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_2 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_2 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_2 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_2 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 3)
                @if($view->hasil_3 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_3 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_3 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_3 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_3 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 4)
                @if($view->hasil_4 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_4 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_4 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_4 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_4 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 5)
                @if($view->hasil_5 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_5 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_5 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_5 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_5 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 6)
                @if($view->hasil_6 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_6 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_6 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_6 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_6 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 7)
                @if($view->hasil_7 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_7 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_7 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_7 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_7 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 8)
                @if($view->hasil_8 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_8 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_8 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_8 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_8 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 9)
                @if($view->hasil_9 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_9 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_9 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_9 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_9 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
            @if($p->id == 10)
                @if($view->hasil_10 == 5)
                    <td colspan="5">{{ ('Sangat Baik') }}</td>
                @elseif($view->hasil_10 == 4)
                    <td colspan="5">{{ ('baik') }}</td>
                @elseif($view->hasil_10 == 3)
                    <td colspan="5">{{ ('Cukup') }}</td>
                @elseif($view->hasil_10 == 2)
                    <td colspan="5">{{  ('Buruk') }}</td>
                @elseif($view->hasil_10 == 1)
                    <td colspan="5">{{  ('Sangat Buruk') }}</td>
                @endif
            @endif
        </tr>
    @endforeach
    <tr>
        <td class="text-center"><b>No</b></td>
        <td class="text-center" colspan="5"><b>Pertanyaan</b></td>
        <td class="text-center" colspan="5"><b>Hasil Jawaban</b></td>
    </tr>
    </tbody>
</table>
<br>
<br>
<br>
<br>
<br>
<p> <label><b>Keterangan :</b></label><br>
    <label><i>5 : Sangat baik <br> 4 : Baik <br> 3 : Cukup <br> 2 : Buruk <br> 1 : Sangat Buruk</i></label>
</p>
<p>
    <label><b>Saran-saran :</b></label><br>
    <label><i>(tidak terbatas pada dosen yang bersangkutan tetapi juga pada pelaksanaan perkuliahan secara keseluruhan)</i></label>
    <label><b></b></label>
</p>


</body>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</html>
