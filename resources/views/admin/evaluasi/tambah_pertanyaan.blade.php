@extends('layouts.app_ekdom')

@section('judul','Tambah Pertanyaan ')


@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('Tambah Pertanyaan Evaluasi Kinerja Dosen') }}</h3>
                                    </div>
                                </div>
                            </div>

                            @include('headerlogin.headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                <div class="widget-content widget-content-area br-6">
                                    <div class="row">
                                        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                            <div class="statbox widget box box-shadow">
                                                <div class="widget-header">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                            <h4>{{ __('Forms Tambah Pertanyaan') }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget-content widget-content-area">
                                                    <form action="{{ url('admin/insert_pertanyaan') }}" method="POST">
                                                        @csrf
                                                        <div class="form-row mb-4">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputEmail4">Noreg</label>
                                                                <input type="text" name="noreg" class="form-control @error('noreg') is-invalid @enderror" id="inputEmail4" placeholder="Noreg" value="{{ Auth::user()->noreg }}" readonly>
                                                                @error('noreg')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="form-row mb-4">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputEmail4">Code Pertanyaan</label>
                                                                <input type="text" name="code_pertanyaan" class="form-control" placeholder="code pertanyaan" value="{{ $pertanyaan }}" readonly>
                                                            </div>
                                                            </div>
                                                            @if($pertanyaan != 'ekdom0011')
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress">Pertanyaan</label>
                                                            <textarea name="pertanyaan" class="form-control" id="pertanyaan" cols="30" rows="10"></textarea>
                                                            <br>
                                                            @error('pertanyaan')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                            @else
                                                                <div class="alert custom-alert-1 mb-4" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" data-dismiss="alert" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close">
                                                                            <line x1="18" y1="6" x2="6" y2="18"></line>
                                                                            <line x1="6" y1="6" x2="18" y2="18"></line>
                                                                        </svg></button>
                                                                    <div class="media">
                                                                        <div class="alert-icon">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell">
                                                                                <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                                                                                <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                                                                            </svg>
                                                                        </div>
                                                                        <div class="media-body">
                                                                            <div class="alert-text">
                                                                                <strong>Pertanyaan Hanya Sampai 10 ! </strong><span>.</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @if($pertanyaan == 'ekdom0011')
                                                            <a href="{{ url('/admin/pertanyaan_evaluasi') }}" class="btn btn-danger mt-3">Kembali</a>
                                                        @else
                                                            <a href="{{ url('/admin/pertanyaan_evaluasi') }}" class="btn btn-danger mt-3">Kembali</a>
                                                            <button type="submit" class="btn btn-primary mt-3">Tambah Data</button>
                                                        @endif
                                                    </form>

                                                    <div class="code-section-container">


                                                        <div class="code-section text-left">
                                                            <pre>
&lt;form&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputEmail4"&gt;Email&lt;/label&gt;
    &lt;input type="email" class="form-control" id="inputEmail4" placeholder="Email"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputPassword4"&gt;Password&lt;/label&gt;
    &lt;input type="password" class="form-control" id="inputPassword4" placeholder="Password"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress"&gt;Address&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St"&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress2"&gt;Address 2&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"&gt;
&lt;/div&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputCity"&gt;City&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputCity"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-4"&gt;
    &lt;label for="inputState"&gt;State&lt;/label&gt;
    &lt;select id="inputState" class="form-control"&gt;
        &lt;option selected&gt;Choose...&lt;/option&gt;
        &lt;option&gt;...&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-2"&gt;
    &lt;label for="inputZip"&gt;Zip&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputZip"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group"&gt;
&lt;div class="form-check pl-0"&gt;
    &lt;div class="custom-control custom-checkbox checkbox-info"&gt;
        &lt;input type="checkbox" class="custom-control-input" id="gridCheck"&gt;
        &lt;label class="custom-control-label" for="gridCheck"&gt;Check me out&lt;/label&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;button type="submit" class="btn btn-primary mt-3"&gt;Sign in&lt;/button&gt;
&lt;/form&gt;
</pre>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    @endsection
