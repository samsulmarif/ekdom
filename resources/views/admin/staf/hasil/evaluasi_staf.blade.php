@extends('layouts.app_ekdom')

@section('judul','Hasil Evaluasi Staf')

@section('content')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="content-container">

                <div class="col-center layout-top-spacing">
                    <div class="col-left-content">

                        <div class="header-container">
                            <header class="header navbar navbar-expand-sm">
                                <div class="d-flex">
                                    <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                        <div class="bt-menu-trigger">
                                            <span></span>
                                        </div>
                                    </a>
                                    <div class="page-header">
                                        <div class="page-title">
                                            <h3>{{ __('Hasil Evaluasi Staf') }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- flash data config nya belakang -->
                            <!-- @if (session('sukses'))
                                <div class="flash-data" data-flashData="{{ session('sukses') }}">
                </div>
            @endif -->
                                @if (session('sukses'))
                                    <div class="alert alert-success">
                                        {{ session('sukses') }}
                                    </div>
                                @endif
                            <!-- buat header login -->
                                @include('headerlogin/headerlogin')
                            </header>
                        </div>

                        <div class="admin-data-content layout-top-spacing">

                            <div class="row layout-top-spacing">
                                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                    <div class="widget-content widget-content-area br-6">
                                        <table id="alter_pagination" class="table table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Noreg</th>
                                                <th>kategori</th>
                                                <th>nim/Nip/NIDN</th>
                                                <th>jurusan</th>
                                                <th>saran</th>
                                                <th class="text-center">View</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($evaluasi as $eva)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $eva->noreg }}</td>
                                                    <td>{{ $eva->kategori }}</td>
                                                    <td>{{ $eva->nim }}</td>
                                                    <td>{{ $eva->jurusan }}</td>
                                                    <td>{{ $eva->saran }}</td>
                                                    <td><a href="{{ url('admin/hasil_evaluasi_staf') }}{{ ('/') }}{{ $eva->id }}{{ ('/') }}{{ ('view') }}" class="btn btn-primary">View</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Noreg</th>
                                                <th>kategori</th>
                                                <th>nim</th>
                                                <th>jurusan</th>
                                                <th>Saran</th>
                                                <th class="text-center">View</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
@endsection
