@extends('layouts.app_ekdom')

@section('judul','Halaman Akun Mahasiswa')

@section('content')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('UsersLists') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- flash data config nya belakang -->
                            <!-- @if (session('sukses'))
                <div class="flash-data" data-flashData="{{ session('sukses') }}">
                </div>
            @endif -->
                            @if (session('sukses'))
                            <div class="alert alert-success">
                                {{ session('sukses') }}
                            </div>
                            @endif
                            <!-- buat header login -->
                            @include('headerlogin/headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                <div class="widget-content widget-content-area br-6">
                                    <table id="alter_pagination" class="table table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>email</th>
                                                <th>Role</th>
                                                <th class="text-center">Update</th>
                                                <th class="text-center">Delete</th>
                                                <th class="text-center">Lacak Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($getMahasiswa as $m)
                                            @if($m->role == 'mahasiswa')
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $m->name }}</td>
                                                <td>{{ $m->email }}</td>
                                                <td>{{ $m->role }}</td>
                                                <td><a href="users/{{ $m->id }}/formsEdit" class="btn btn-primary">Edit</a></td>
                                                <form action="users/{{ $m->id }}/delete" method="POST">
                                                    @method('delete')
                                                    @csrf
                                                    <td class="text-center">
                                                        <button class="btn btn-danger">Hapus</button>
                                                    </td>
                                                </form>
                                                <td><a href="{{ url('admin/lacak_status') }}{{ ('/') }}{{ $m->id }}{{ ('/') }}{{ ('mahasiswa') }}" class="btn btn-primary">Lacak Status</a></td>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>email</th>
                                                <th class="text-center">Update</th>
                                                <th class="text-center">Delete</th>
                                                <th class="text-center">Lacak Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endsection
