@extends('layouts.app_ekdom')

@section('judul','Halaman Akun Dosen')

@section('content')
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('UsersLists') }}</h3>
                                    </div>
                                </div>
                            </div>
                            <!-- flash data config nya belakang -->
                            <!-- @if (session('sukses'))
                <div class="flash-data" data-flashData="{{ session('sukses') }}">
                </div>
            @endif -->
                            @if (session('sukses'))
                            <div class="alert alert-success">
                                {{ session('sukses') }}
                            </div>
                            @endif
                            <!-- buat header login -->
                            @include('headerlogin/headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                <div class="widget-content widget-content-area br-6">
                                    <table id="alter_pagination" class="table table-hover" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>email</th>
                                                <th>Role</th>
                                                <th class="text-center">Update</th>
                                                <th class="text-center">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($getDosen as $d)
                                            @if($d->role == 'dosen')
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $d->name }}</td>
                                                <td>{{ $d->email }}</td>
                                                <td>{{ $d->role }}</td>
                                                <td><a href="users/{{ $d->id }}/formsEdit" class="btn btn-primary">Edit</a></td>
                                                <form action="users/{{ $d->id }}/delete" method="POST">
                                                    @method('delete')
                                                    @csrf
                                                    <td class="text-center">
                                                        <button class="btn btn-danger">Hapus</button>
                                                    </td>
                                                </form>
                                            </tr>
                                            @endif
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>email</th>
                                                s <th class="text-center">Update</th>
                                                <th class="text-center">Delete</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endsection
