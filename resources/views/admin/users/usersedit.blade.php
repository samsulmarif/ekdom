@extends('layouts.app_ekdom')

@section('judul','Edit Users')


@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('UsersEdit') }}</h3>
                                    </div>
                                </div>
                            </div>

                            <div class="header-actions">
                                <div class="nav-item dropdown language-dropdown">
                                    <div class="dropdown custom-dropdown-icon">
                                        <a class="dropdown-toggle btn" href="#" role="button" id="customDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('uho.png')}}" width="100" height="100" alt="flag"><span>{{ Auth::user()->name }}</span></a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customDropdown">
                                            <a class="dropdown-item" href="{{ url('/profile') }}">Profile</a>
                                            <form action="{{ route('logout') }}" method="POST">
                                                @csrf
                                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">{{ __('Logout')}}</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="toggle-notification-bar">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell">
                                        <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                                        <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                                    </svg>
                                </div>
                            </div>
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                <div class="widget-content widget-content-area br-6">
                                    <div class="row">
                                        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                            <div class="statbox widget box box-shadow">
                                                <div class="widget-header">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                            <h4>{{ __('Forms Edit Users') }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget-content widget-content-area">
                                                    <form action="{{ url('admin') }}{{ ('/') }}{{ ('users') }}{{ ('/') }}{{ $modelsUsers->id }}{{ ('/') }}{{ ('edit-data') }}" method="POST">
                                                        @method('put')
                                                        @csrf
                                                        <div class="form-row mb-4">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputEmail4">Email</label>
                                                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="inputEmail4" placeholder="Email" value="{{ $modelsUsers->email }}">
                                                                @error('email')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPassword4">Nama Lengkap</label>
                                                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="inputPassword4" placeholder="Nama Lengkap" value="{{ $modelsUsers->name }}">
                                                                @error('nama')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress">password</label>
                                                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="inputAddress">
                                                            @error('password')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">konfirmasi password</label>
                                                            <input type="password" name="conf-password" class="form-control @error('conf-password') is-invalid @enderror" id="inputAddress2">
                                                            @error('conf-password')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputEmail4">Noreg</label>
                                                            <input type="text" class="form-control" name="noreg" value="{{ $modelsUsers->noreg }}" readonly>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary mt-3">Edit Data</button>
                                                    </form>

                                                    <div class="code-section-container">

                                                        <div class="code-section text-left">
                                                            <pre>
&lt;form&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputEmail4"&gt;Email&lt;/label&gt;
    &lt;input type="email" class="form-control" id="inputEmail4" placeholder="Email"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputPassword4"&gt;Password&lt;/label&gt;
    &lt;input type="password" class="form-control" id="inputPassword4" placeholder="Password"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress"&gt;Address&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St"&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress2"&gt;Address 2&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"&gt;
&lt;/div&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputCity"&gt;City&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputCity"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-4"&gt;
    &lt;label for="inputState"&gt;State&lt;/label&gt;
    &lt;select id="inputState" class="form-control"&gt;
        &lt;option selected&gt;Choose...&lt;/option&gt;
        &lt;option&gt;...&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-2"&gt;
    &lt;label for="inputZip"&gt;Zip&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputZip"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group"&gt;
&lt;div class="form-check pl-0"&gt;
    &lt;div class="custom-control custom-checkbox checkbox-info"&gt;
        &lt;input type="checkbox" class="custom-control-input" id="gridCheck"&gt;
        &lt;label class="custom-control-label" for="gridCheck"&gt;Check me out&lt;/label&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;button type="submit" class="btn btn-primary mt-3"&gt;Sign in&lt;/button&gt;
&lt;/form&gt;
</pre>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    @endsection