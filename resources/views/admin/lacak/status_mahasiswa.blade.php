@extends('layouts.app_ekdom')

@section('judul','Time Line Status Mahasiswa')


@section('content')

    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="content-container">

                <div class="col-left layout-top-spacing">
                    <div class="col-left-content">

                        <div class="header-container">
                            <header class="header navbar navbar-expand-sm">
                                <div class="d-flex">
                                    <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                        <div class="bt-menu-trigger">
                                            <span></span>
                                        </div>
                                    </a>
                                    <div class="page-header">
                                        <div class="page-title">
                                            <h3>{{ __('lacak status mahasiswa') }}</h3>
                                        </div>
                                    </div>
                                </div>

                               @include('headerlogin.headerlogin')
                            </header>
                        </div>

                        <div class="admin-data-content layout-top-spacing">

                            <div class="row layout-top-spacing">
                                <div id="timelineProfile" class="col-lg-12 layout-spacing">
                                    <div class="statbox widget box box-shadow">
                                        <div class="widget-header">
                                            <div class="row">
                                                <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                    <h4> Profile</h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="widget-content widget-content-area">
                                            <div class="timeline-simple">
                                                <p class="timeline-title">Status Timeline Mahasiswa</p>

                                                <div class="timeline-list">

                                                    <div class="timeline-post-content">
                                                        <div class="user-profile">
                                                            <img src="{{ asset('assets/img/uho.png') }}" alt="">
                                                        </div>
                                                        <div class="">
                                                            <h4>{{ $status_mahasiswa->name  }} - {{ $status_mahasiswa->noreg }}</h4>
                                                            <p class="meta-time-date">{{ $status_mahasiswa->created_at }}</p>
                                                            <div class="">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
                                                                <h6 class="">{{ ('Akun') }} {{ $status_mahasiswa->name }} {{ ('Success Created By Sistem') }}</h6>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="timeline-post-content">
                                                        <div class="user-profile">
                                                            <img src="{{ asset('assets/img/uho.png') }}" alt="">
                                                        </div>
                                                        <div class="">
                                                            <h4>{{ $status_mahasiswa->name  }} - {{ $status_mahasiswa->noreg }}</h4>
                                                            <p class="meta-time-date">{{ $status_mahasiswa->created_at }}</p>
                                                            <div class="">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
                                                                <h6 class="">Silahkan Login</h6>
                                                                <p class="post-text">Lengkapi Biodata</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @if($status_mahasiswa->biodata == 'Y')
                                                        <div class="timeline-post-content">
                                                            <div class="user-profile">
                                                                <img src="{{ asset('assets/img/uho.png') }}" alt="">
                                                            </div>
                                                            <div class="">
                                                                <h4>{{ $status_mahasiswa->name  }} - {{ $status_mahasiswa->noreg }}</h4>
                                                                <p class="meta-time-date">{{ $status_mahasiswa->created_at }}</p>
                                                                <div class="">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
                                                                    <h6 class="">Sukses Mengisi Biodata</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="timeline-post-content">
                                                            <div class="user-profile">
                                                                <img src="{{ asset('assets/img/uho.png') }}" alt="">
                                                            </div>
                                                            <div class="">
                                                                <h4>{{ $status_mahasiswa->name  }} - {{ $status_mahasiswa->noreg }}</h4>
                                                                <p class="meta-time-date">{{ $status_mahasiswa->created_at }}</p>
                                                                <div class="">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
                                                                    <h6 class="">Belum Mengisi Evaluasi Harap Isi Evaluasinya </h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif

                                                    @if($status_mahasiswa->evaluasi == 'Y')
                                                        <div class="timeline-post-content">
                                                            <div class="user-profile">
                                                                <img src="{{ asset('assets/img/uho.png') }}" alt="">
                                                            </div>
                                                            <div class="">
                                                                <h4>{{ $status_mahasiswa->name  }} - {{ $status_mahasiswa->noreg }}</h4>
                                                                <p class="meta-time-date">{{ $status_mahasiswa->created_at }}</p>
                                                                <div class="">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-globe"><circle cx="12" cy="12" r="10"></circle><line x1="2" y1="12" x2="22" y2="12"></line><path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path></svg>
                                                                    <h6 class="">Sukses Kirim Evaluasi</h6>
                                                                    <p class="post-text">Terimakasih Telah Mengirim Evaluasi Kinerja Dosen</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="timeline-post-content">
                                                            <div class="user-profile">
                                                                <img src="{{ asset('assets/img/uho.png') }}" alt="">
                                                            </div>
                                                            <div class="">
                                                                <h4>{{ ('Selesai') }}</h4>
                                                                <p class="meta-time-date">{{ $status_mahasiswa->created_at }}</p>
                                                            </div>
                                                        </div>
                                                    @endif

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!--  END CONTENT AREA  -->

@endsection
