@extends('layouts.app_ekdom')

@section('judul','Halaman Dashboard')

@section('content')
<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>Dashboard Administrator</h3>
                                    </div>
                                </div>
                            </div>

                            @include('headerlogin.headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row">

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-one">
                                    <div class="widget-heading">
                                        <h6 class="">Statistics</h6>
                                    </div>
                                    <div class="w-chart">

                                        <div class="w-chart-section total-visits-content">
                                            <div class="w-detail">
                                                <p class="w-title">Total Akun Mahasiswa</p>
                                                <p class="w-stats">{{ $mahasiswa }}</p>
                                            </div>
                                            <div class="w-chart-render-one">
                                                <div id="total-users"></div>
                                            </div>
                                        </div>


                                        <div class="w-chart-section paid-visits-content">
                                            <div class="w-detail">
                                                <p class="w-title">Total Akun Dosen</p>
                                                <p class="w-stats">{{ $dosen }}</p>
                                            </div>
                                            <div class="w-chart-render-one">
                                                <div id="paid-visits"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-account-invoice-two">
                                    <div class="widget-content">
                                        <div class="account-box">
                                            <div class="info">
                                                <div class="inv-title">
                                                    <h5 class="">Total Data Evaluasi Kinerja Staf</h5>
                                                </div>
                                                <div class="inv-balance-info">

                                                    <p class="inv-balance">{{ $evaluasi_kinerja_staf }}</p>

                                                </div>
                                            </div>
                                            <div class="acc-action">
                                                <a href="javascript:void(0);">Lihat Data</a>
                                            </div>
                                            <div class="info">
                                                <div class="inv-title">
                                                    <h5 class="">Total Data Evaluasi Prasarana</h5>
                                                </div>
                                                <div class="inv-balance-info">

                                                    <p class="inv-balance">{{ $prasarana }}</p>

                                                </div>
                                            </div>
                                            <div class="acc-action">
                                                <a href="{{ url('admin/hasil_evaluasi_prasarana') }}">Lihat Data</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endsection
