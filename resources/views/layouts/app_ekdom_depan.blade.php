<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>@yield('judul')</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/Logo-UHO.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/Logo-UHO.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/Logo-UHO.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/Logo-UHO.png') }}">
    <link rel="manifest" href="{{ asset('front/assets/img/favicons/manifest.json') }}">
    <meta name="msapplication-TileImage" content="{{ asset('front/assets/img/favicons/mstile-150x150.png') }}">
    <meta name="theme-color" content="#ffffff">


    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="{{ asset('front/assets/css/theme.css') }}" rel="stylesheet" />
</head>

<body>
@yield('konten')


    <!-- ===============================================-->
    <!--    JavaScripts-->
    <!-- ===============================================-->
    <script src="{{ asset('front/vendors/@popperjs/popper.min.js') }}"></script>
    <script src="{{ asset('front/vendors/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front/vendors/is/is.min.js') }}"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
    <script src="{{ asset('front/vendors/fontawesome/all.min.js') }}"></script>
    <script src="{{ asset('front/assets/js/theme.js') }}"></script>

    <link href="https://fonts.googleapis.com/css2?family=DM+Serif+Display&amp;family=Rubik:wght@300;400;500;600;700;800&amp;display=swap" rel="stylesheet">
    </body>

</html>

</head>
