<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>@if(Route::has('login')) @auth @yield('judul') @else @yield('judul') @endauth @endif</title>
    <!-- jika sdh login maka judul dari halaman adshboard yang di eksekusi , tapi kalo belom login maka judul
dari halaman front end yang di eksekusi-->
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/Logo-UHO.png') }}" />
    <link href="{{ asset('assets/css/loader.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/js/loader.js') }}"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/structure.css') }}" rel="stylesheet" type="text/css" class="structure" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="{{ asset('plugins/apex/apexcharts.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/dashboard/dash_1.css') }}" rel="stylesheet" type="text/css" class="dashboard-analytics" />
    <link href="{{ asset('assets/css/authentication/form-1.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/forms/theme-checkbox-radio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/forms/switches.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/datatables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/table/datatable/dt-global_style.css') }}">

    <link href="{{ asset('assets/css/scrollspyNav.css') }}" rel="stylesheet" type="text/css" />

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{ asset('assets/css/users/user-profile.css') }}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <!-- error css 404 -->
    <link href="{{ asset('assets/css/pages/error/style-400.css') }}" rel="stylesheet" type="text/css" />

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('plugins/flatpickr/flatpickr.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/noUiSlider/nouislider.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/flatpickr/custom-flatpickr.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/noUiSlider/custom-nouiSlider.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/bootstrap-range-Slider/bootstrap-slider.css') }}" rel="stylesheet" type="text/css">
    <!-- END THEME GLOBAL STYLES -->

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/elements/alert.css') }}">

    <!-- css timeline-->
    <link href="{{ asset('assets/css/components/timeline/custom-timeline.css') }}" rel="stylesheet" type="text/css" />

    <style>
        /* User Profile Dropdown*/
        .admin-header .header-container .nav-item.user-profile-dropdown {
            align-self: center;
            padding: 0;
            border-radius: 8px;
            margin-left: 22px;
            margin-right: 0;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-toggle {
            display: flex;
            justify-content: flex-end;
            padding: 0 20px 0 16px;
            transition: .5s;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-toggle:after {
            display: none;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-toggle svg {
            color: #515365;
            width: 15px;
            height: 15px;
            align-self: center;
            margin-left: 6px;
            stroke-width: 1.7px;
            -webkit-transition: -webkit-transform .2s ease-in-out;
            transition: -webkit-transform .2s ease-in-out;
            transition: transform .2s ease-in-out;
            transition: transform .2s ease-in-out, -webkit-transform .2s ease-in-out;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown.show .dropdown-toggle svg {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }

        .admin-header .header-container .nav-item.user-profile-dropdown a.user .media {
            margin: 0;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown a.user .media img {
            width: 33px;
            height: 33px;
            border-radius: 6px;
            box-shadow: 0 0px 0.9px rgba(0, 0, 0, 0.07), 0 0px 7px rgba(0, 0, 0, 0.14);
            margin-right: 13px;
            border: none;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown a.user .media .media-body {
            flex: auto;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown a.user .media .media-body h6 {
            color: #4361ee;
            font-size: 13px;
            font-weight: 600;
            margin-bottom: 0;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown a.user .media .media-body h6 span {
            color: #515365;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown a.user .media .media-body p {
            color: #bfc9d4;
            font-size: 10px;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .nav-link.user {
            padding: 0 0;
            font-size: 25px;
        }

        .admin-header .header-container .nav-item.dropdown.user-profile-dropdown .nav-link:after {
            display: none;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu {
            z-index: 9999;
            max-width: 13rem;
            padding: 0 11px;
            top: 166% !important;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu.show {
            top: 42px !important;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .user-profile-section {
            padding: 16px 14px;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            margin-right: -12px;
            margin-left: -12px;
            background: rgb(96 152 149);
            margin-top: -1px;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .user-profile-section .media {
            margin: 0;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .user-profile-section .media img {
            width: 38px;
            height: 38px;
            border-radius: 50%;
            border: 3px solid rgb(224 230 237 / 58%);
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .user-profile-section .media .media-body {
            align-self: center;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .user-profile-section .media .media-body h5 {
            font-size: 14px;
            font-weight: 500;
            margin-bottom: 0;
            color: #fafafa;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .user-profile-section .media .media-body p {
            font-size: 11px;
            font-weight: 500;
            margin-bottom: 0;
            color: #e0e6ed;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item {
            padding: 0;
            background: transparent;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item a {
            display: block;
            color: #3b3f5c;
            font-size: 13px;
            font-weight: 600;
            padding: 9px 14px;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item a:hover {
            color: #000
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item.active,
        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item:active {
            background-color: transparent;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item:not(:last-child) {
            border-bottom: 1px solid #ebedf2;
        }

        .admin-header .header-container .nav-item.user-profile-dropdown .dropdown-menu .dropdown-item svg {
            width: 17px;
            margin-right: 7px;
            height: 17px;
            color: #009688;
            fill: rgb(0 150 136 / 13%);
        }

        #content .col-left {
            margin-right: 0;
        }

        /*
            The below code is for DEMO purpose --- Use it if you are using this demo otherwise, Remove it
        */
        .navbar .navbar-item.navbar-dropdown {
            margin-left: auto;
        }

        .layout-px-spacing {
            min-height: calc(100vh - 145px) !important;
        }
    </style>

</head>

@if(Route::has('login'))
<!-- session login -->
@auth
<!-- jika sdh login maka header , footer dan content admin yang di eksekusi-->
@include('header.header')
@yield('content')
@include('footer.footer')
@else
<!-- jika belom login maka content frontend yang eksekusi-->
@yield('konten')
@endauth
@endif

<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('assets/js/libs/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('bootstrap/js/popper.min.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('assets/js/app.js') }}"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="{{ asset('assets/js/custom.js') }}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="{{ asset('plugins/apex/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/js/dashboard/dash_1.js') }}"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->
<script src="{{ asset('assets/js/authentication/form-1.js') }}"></script>
<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

<script src="{{ asset('assets/js/scrollspyNav.js') }}"></script>
<script src="{{ asset('plugins/highlight/highlight.pack.js') }}"></script>

<script src="{{ asset('plugins/table/datatable/datatables.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#alter_pagination').DataTable({
            "pagingType": "full_numbers",
            "dom": "<'dt--top-section'<'row'<'col-12 col-sm-6 d-flex justify-content-sm-start justify-content-center'l><'col-12 col-sm-6 d-flex justify-content-sm-end justify-content-center mt-sm-0 mt-3'f>>>" +
                "<'table-responsive'tr>" +
                "<'dt--bottom-section d-sm-flex justify-content-sm-between text-center'<'dt--pages-count  mb-sm-0 mb-3'i><'dt--pagination'p>>",
            "oLanguage": {
                "oPaginate": {
                    "sFirst": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
                    "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                    "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                    "sLast": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
                },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7
        });
    });
</script>

<script src="{{ asset('plugins/flatpickr/custom-flatpickr.js') }}"></script>
<script src="{{ asset('plugins/noUiSlider/custom-nouiSlider.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-range-Slider/bootstrap-rangeSlider.js') }}"></script>
<script src="{{ asset('plugins/flatpickr/flatpickr.js') }}"></script>
<script src="{{ asset('plugins/noUiSlider/nouislider.min.js') }}"></script>
<!-- <script src="{{ asset('assets/js/myalert.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.all.min.js') }}"></script> -->

</body>

</html>
