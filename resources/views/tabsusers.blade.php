<div class="header-actions">
    <div class="nav-item dropdown language-dropdown">
        <div class="dropdown custom-dropdown-icon">
            <a class="dropdown-toggle btn" href="#" role="button" id="customDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('uho.png')}}" width="100" height="100" alt="flag"><span>{{ Auth::user()->name }}</span></a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="customDropdown">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">{{ __('Logout')}}</a>
                </form>
            </div>
        </div>
    </div>

    <div class="toggle-notification-bar">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell">
            <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
            <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
        </svg>
    </div>
</div>