@extends('layouts.app_ekdom')

@section('judul','Evaluasi Kinerja Staf Dan Prasarana')

@section('content')

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="content-container">

                <div class="col-center layout-top-spacing">
                    <div class="col-left-content">

                        <div class="header-container">
                            <header class="header navbar navbar-expand-sm">
                                <div class="d-flex">
                                    <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                        <div class="bt-menu-trigger">
                                            <span></span>
                                        </div>
                                    </a>
                                    <div class="page-header">
                                        <div class="page-title">
                                            <h3>{{ __('Pertanyaan Evaluasi Staf Dan Prasarana') }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- flash data config nya belakang -->
                            <!-- @if (session('sukses'))
                                <div class="flash-data" data-flashData="{{ session('sukses') }}">
                </div>
            @endif -->
                                <!-- buat header login -->
                                @include('headerlogin/headerlogin')
                            </header>
                        </div>
                        @if(session('sukses'))
                            <div class="alert custom-alert-1 mb-4" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" data-dismiss="alert" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close">
                                        <line x1="18" y1="6" x2="6" y2="18"></line>
                                        <line x1="6" y1="6" x2="18" y2="18"></line>
                                    </svg></button>
                                <div class="media">
                                    <div class="alert-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell">
                                            <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                                            <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                                        </svg>
                                    </div>
                                    <div class="media-body">
                                        <div class="alert-text">
                                            <strong>Sukses! </strong><span> {{ session('sukses') }}.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="admin-data-content layout-top-spacing">

                            <div class="row layout-top-spacing">
                                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                    <div class="widget-content widget-content-area br-6">
                                        <table id="alter_pagination" class="table table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>noreg</th>
                                                <th>kategori</th>
                                                <th class="text-center">Evaluasi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($evaluasi as $e)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $e->noreg }}</td>
                                                    <td>{{ $e->evaluasi }}</td>
                                                    <td><a href="{{ url('/dosen/kirim_evaluasi_staf') }}" class="btn btn-primary">Mulai Evaluasi</a></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>noreg</th>
                                                <th>nilai</th>
                                                <th class="text-center">Evaluasi</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

@endsection
