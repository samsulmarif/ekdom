@extends('layouts.app_ekdom')

@section('judul','Halaman Dashboard')

@section('content')
<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>Dosen Dashboard</h3>
                                    </div>
                                </div>
                            </div>

                            @include('tabsusers')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row">

                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-card-four">
                                    <div class="widget-content">
                                        <div class="w-header">
                                            <div class="w-info">
                                                <h6 class="value">Total Dosen</h6>
                                            </div>
                                            <div class="task-action">
                                            </div>
                                        </div>


                                        <div class="w-content">

                                            <div class="w-info">
                                                <p class="value">{{ $data_dosen }} <span>Yang Aktif</span> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trending-up">
                                                        <polyline points="23 6 13.5 15.5 8.5 10.5 1 18"></polyline>
                                                        <polyline points="17 6 23 6 23 12"></polyline>
                                                    </svg></p>
                                                <!-- <p class="">Expenses</p> -->
                                            </div>

                                        </div>

                                        <div class="w-progress-stats">
                                            <div class="progress">
                                                <div class="progress-bar bg-gradient-secondary" role="progressbar" style="width: {{ $data_dosen }}%" aria-valuenow="{{ $data_dosen }}" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>

                                            <div class="">
                                                <div class="w-icon">
                                                    <p>{{ $data_dosen }}</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12 layout-spacing">
                                <div class="widget widget-activity-five">

                                    <div class="widget-heading">
                                        <h5 class="">{{ __('status log yang mengirim evaluasi') }}</h5>
                                    </div>

                                    <div class="widget-content">

                                        <div class="w-shadow-top"></div>
                                        <div class="mt-container mx-auto">
                                            <div class="timeline-line">

                                                @foreach($evaluasi as $eva)
                                                    @if($eva->role == 'mahasiswa')
                                                <div class="item-timeline timeline-new">
                                                    <div class="t-dot">
                                                        <div class="t-secondary"><svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg></div>
                                                    </div>
                                                    <div class="t-content">
                                                        <div class="t-uppercontent">
                                                            <h5>{{ $eva->name }}: <a href="javscript:void(0);"><span>[@if($eva->evaluasi != 'Y'){{ __('belum mengirim evaluasi') }} @else {{ __('sudah mengirim evaluasi') }} @endif]</span></a></h5>
                                                        </div>
                                                        <p>{{ $eva->created_at }}</p>
                                                    </div>
                                                </div>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>

                                        <div class="w-shadow-bottom"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    @endsection
