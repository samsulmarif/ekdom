@extends('layouts.app_ekdom')

@section('judul','Profile Dosen')

@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-left layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('Profile Dosen') }}</h3>
                                    </div>
                                </div>
                            </div>
                            @include('tabsusers')
                        </header>
                    </div>

                    @if (session('sukses'))
                    <div class="alert alert-success">
                        {{ session('sukses') }}
                    </div>
                    @endif

                    @if (session('gagal'))
                        <div class="alert alert-danger">
                            {{ session('gagal') }}
                        </div>
                    @endif

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-spacing">

                            <!-- Content -->
                            <div class="col-xl-5 col-lg-4 col-md-4 col-sm-12 layout-top-spacing">

                                <div class="user-profile layout-spacing">
                                    <div class="widget-content widget-content-area">

                                        @foreach($getprofile as $p)
                                        @if(Auth::user()->noreg == $p->noreg)
                                        <div class="d-flex justify-content-between">
                                            <h3 class="">{{ ('BioData Diri') }}</h3>
                                            <a href="{{ url('/dosen') }}{{ ('/') }}{{ ('profile') }}{{ ('/') }}{{ $p->id }}{{ ('/') }}{{ ('dosen') }}" class="mt-2 edit-profile"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3">
                                                    <path d="M12 20h9"></path>
                                                    <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                                </svg></a>
                                        </div>
                                        <div class="text-center user-info">
                                            <img src="{{ asset('/assets/img/uho.png') }}" alt="uho-png-profile" class="src" width="90" height="90">
                                            <p class="">{{ $p->name }}</p>
                                        </div>
                                        <div class="user-info-list">

                                            <div class="">
                                                <ul class="contacts-block list-unstyled">
                                                    <li class="contacts-block__item">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-coffee">
                                                            <path d="M18 8h1a4 4 0 0 1 0 8h-1"></path>
                                                            <path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path>
                                                            <line x1="6" y1="1" x2="6" y2="4"></line>
                                                            <line x1="10" y1="1" x2="10" y2="4"></line>
                                                            <line x1="14" y1="1" x2="14" y2="4"></line>
                                                        </svg> @if($p->hobi != null)
                                                        {{ ('Hobi :') }}{{ $p->hobi }}
                                                        @else
                                                        {{ __('hobi belum ada (harap isi dulu)') }}
                                                        @endif
                                                    </li>
                                                    <li class="contacts-block__item">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar">
                                                            <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                                            <line x1="16" y1="2" x2="16" y2="6"></line>
                                                            <line x1="8" y1="2" x2="8" y2="6"></line>
                                                            <line x1="3" y1="10" x2="21" y2="10"></line>
                                                        </svg>{{ ('Jabatan :') }}{{ $p->role }}
                                                    </li>
                                                    <li class="contacts-block__item">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin">
                                                            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                                                            <circle cx="12" cy="10" r="3"></circle>
                                                        </svg>@if($p->alamat != null)
                                                        {{ ('Alamat :') }}{{ $p->alamat }}
                                                        @else
                                                        {{ __('alamat belum ada (harap isi dulu)') }}
                                                        @endif
                                                    </li>
                                                    <li class="contacts-block__item">
                                                        <a href="mailto:example@mail.com"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail">
                                                                <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                                                                <polyline points="22,6 12,13 2,6"></polyline>
                                                            </svg>{{ $p->email }}</a>
                                                    </li>
                                                    <li class="contacts-block__item">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone">
                                                            <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                                        </svg> @if($p->no_wa != null)
                                                        {{ ('No Hp/Whatsapp :') }}{{ $p->no_wa }}
                                                        @else
                                                        {{ __('No Wa belum ada (harap isi dulu)') }}
                                                        @endif
                                                    </li>
                                                    <li class="contacts-block__item">
                                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                                            <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                                            <circle cx="8.5" cy="7" r="4"></circle>
                                                            <polyline points="17 11 19 13 23 9"></polyline>
                                                        </svg> @if($p->Nim_Nidn_Nip != null)
                                                        {{ ('Nip/NIDN :') }}{{ $p->Nim_Nidn_Nip }}
                                                        @else
                                                        {{ __('Nip belum ada (harap isi dulu)') }}
                                                        @endif
                                                    </li>
                                                    <li class="contacts-block__item">
                                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                                            <path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                                            <circle cx="8.5" cy="7" r="4"></circle>
                                                            <polyline points="17 11 19 13 23 9"></polyline>
                                                        </svg> @if($p->jurusan != null)
                                                        {{ ('Jurusan :') }}{{ $p->jurusan }}
                                                        @else
                                                        {{ __('Jurusan belum ada (harap isi dulu)') }}
                                                        @endif
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </div>

                            </div>

                            @foreach($getprofile as $p)
                            @if(Auth::user()->noreg == $p->noreg)
                            <div class="col-xl-7 col-lg-8 col-md-8 col-sm-12 layout-top-spacing">

                                <div class="bio layout-spacing ">
                                    <div class="widget-content widget-content-area">
                                        <h3 class="">Tentang Diri</h3>
                                        <p>@if($p->about != null)
                                            {{ $p->about }}
                                            @else
                                            {{ __('data about anda masih kosong harap isi') }}
                                            @endif
                                        </p>

                                    </div>
                                </div>

                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!--  END CONTENT AREA  -->
@endsection
