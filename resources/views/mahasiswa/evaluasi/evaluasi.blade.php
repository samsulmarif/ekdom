@extends('layouts.app_ekdom')

@section('judul','Evaluasi Kinerja Dosen')

@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-left layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>Kuesioner Kinerja Dosen</h3>
                                    </div>
                                </div>
                            </div>

                            @include('headerlogin.headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div id="btnGroupHorizontal" class="col-lg-12 layout-spacing">
                                <div class="statbox widget box box-shadow">
                                    <div class="widget-header">
                                        <div id="tableCheckbox" class="col-lg-12 col-12 layout-spacing">
                                            <form action="{{ url('mahasiswa/insert_evaluasi') }}" method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                        @foreach($evaluasi as $e)
                                                        <h4>
                                                            <table class="table table-bordered" style="margin-bottom: 0px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Kategori :</td>
                                                                        <td><input type="text" value="{{ $e->evaluasi }}" name="kategori" id="dosen" class="form-control" readonly></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Noreg Mahasiswa:</td>
                                                                        <td><input type="text" value="{{ Auth::user()->noreg }}" name="noreg_mahasiswa" id="dosen" class="form-control" readonly></td>
                                                                    </tr> <tr>
                                                                        <td>Nama Mahasiswa:</td>
                                                                        <td><input type="text" value="{{ Auth::user()->name }}" name="nama_mahasiswa" id="dosen" class="form-control" readonly></td>
                                                                    </tr>
                                                                    </tr> <tr>
                                                                        <td>Jurusan:</td>
                                                                        <td><input type="text" value="{{ Auth::user()->jurusan }}" name="jurusan" id="mahasiswa" class="form-control" readonly></td>
                                                                    </tr>
                                                                    </tr> <tr>
                                                                        <td>Nim:</td>
                                                                        <td><input type="text" value="{{ Auth::user()->Nim_Nidn_Nip }}" name="nim" id="mahasiswa" class="form-control" readonly></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </h4>
                                                        @endforeach

                                                        <!-- keterangan nilai -->
                                                        <blockquote class="blockquote media-object">
                                                            <div class="media">
                                                                <div class="media-body align-self-center">
                                                                    <label><b>Keterangan Nilai :</b></label>
                                                                    <br><label><br> 5 = sangat baik <br> 4 =
                                                                        baik <br> 3
                                                                        = cukup
                                                                        <br> 2 =
                                                                        buruk <br> 1 = sangat buruk</label>
                                                                </div>
                                                            </div>
                                                        </blockquote>

                                                        <h4>
                                                            <p>Berikut penilaian mahasiswa terhadap dosen dengan
                                                                cara
                                                                memberi tanda (<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                                                    <polyline points="20 6 9 17 4 12">
                                                                    </polyline>
                                                                </svg>) pada kolom</p>
                                                        </h4>
                                                    </div>
                                                </div>

                                                <div class="widget-content widget-content-area">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-hover table-striped table-checkable table-highlight-head mb-4">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center" rowspan="2">No</th>
                                                                    <th class="text-center" rowspan="2">Materi
                                                                        Penilaian</th>
                                                                    <th class="text-center" colspan="5">Nilai
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    @foreach($evaluasi as $ev)
                                                                    @for($a=1; $a<=$ev->nilai; $a++)
                                                                        <th class="text-center"> {{ $a }} </th>
                                                                        @endfor
                                                                        @endforeach
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($pertanyaan as $p)
                                                                <tr>
                                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                                    <td>
                                                                        <p class="mb-0">{{ $p->pertanyaan }}</p>
                                                                    </td>
                                                                    @foreach($evaluasi as $eva)
                                                                        @for($a=1; $a<=$eva->nilai; $a++)
                                                                            <td class="text-center checkbox-column">
                                                                                <label class="new-control new-checkbox checkbox-primary" style="height: 18px; margin: 0 auto;">
                                                                                    <input type="checkbox" name="{{ $p->code_pertanyaan }}" class="new-control-input todochkbox" id="kategori" value="{{ $a }}">
                                                                                    <span class="new-control-indicator"></span>
                                                                                </label>
                                                                            </td>
                                                                        @endfor
                                                                    @endforeach
                                                                </tr>
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>


                                                <!-- input kometar -->
                                                <div class="col-md-10">
                                                    <label><b>Saran-saran :</b></label>
                                                    <label>(tidak terbatas pada dosen yang bersangkutan tetapi
                                                        juga pada pelaksanaan perkuliahan secara
                                                        keseluruhan)</label>
                                                </div>
                                                <div class="col-md-12">
                                                    <textarea name="saran" class="form-control @error('saran') @enderror" aria-label="With textarea" placeholder="Komentar anda ..."></textarea>
                                                    <br>
                                                    @error('saran')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>

                                                <button type="submit" class="btn btn-primary mt-3"><svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                                        <path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z">
                                                        </path>
                                                        <polyline points="17 21 17 13 7 13 7 21"></polyline>
                                                        <polyline points="7 3 7 8 15 8"></polyline>
                                                    </svg> Simpan</button>
                                                <button type="reset" class="btn btn-danger mt-3"><svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                                        <polyline points="1 4 1 10 7 10"></polyline>
                                                        <polyline points="23 20 23 14 17 14"></polyline>
                                                        <path d="M20.49 9A9 9 0 0 0 5.64 5.64L1 10m22 4l-4.64 4.36A9 9 0 0 1 3.51 15">
                                                        </path>
                                                    </svg> Reset Data</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!--  END CONTENT AREA  -->
@endsection
