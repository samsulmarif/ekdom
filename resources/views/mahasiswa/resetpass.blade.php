@extends('layouts.app_ekdom')

@section('judul','Update Password')


@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('Reset Password') }}</h3>
                                    </div>
                                </div>
                            </div>

                            @include('tabsusers')

                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                <div class="widget-content widget-content-area br-6">
                                    <div class="row">
                                        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                            <div class="statbox widget box box-shadow">
                                                <div class="widget-header">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                            <h4>{{ __('Reset Password') }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget-content widget-content-area">
                                                    <form action="{{ url('/mahasiswa') }}{{ ('/') }}{{ ('reset_pass') }}{{ ('/') }}{{ $id_pass->id }}{{ ('/') }}{{ ('ubah') }}" method="POST">
                                                        @method('put')
                                                        @csrf
                                                        <div class="form-row mb-4">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputEmail4">Password</label>
                                                                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="inputEmail4" placeholder="Password">
                                                                @error('password')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPassword4">Konfirmasi Password</label>
                                                                <input type="password" name="conf_pass" class="form-control @error('conf_pass') is-invalid @enderror" id="inputPassword4" placeholder="Konfirmasi Password">
                                                                @error('conf_pass')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary mt-3">Ubah Password</button>
                                                        <a href="{{ url('/mahasiswa/profile/view') }}" class="btn btn-success mt-3">Kembali</a>
                                                    </form>

                                                    <div class="code-section-container">


                                                        <div class="code-section text-left">
                                                            <pre>
&lt;form&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputEmail4"&gt;Email&lt;/label&gt;
    &lt;input type="email" class="form-control" id="inputEmail4" placeholder="Email"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputPassword4"&gt;Password&lt;/label&gt;
    &lt;input type="password" class="form-control" id="inputPassword4" placeholder="Password"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress"&gt;Address&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St"&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress2"&gt;Address 2&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"&gt;
&lt;/div&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputCity"&gt;City&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputCity"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-4"&gt;
    &lt;label for="inputState"&gt;State&lt;/label&gt;
    &lt;select id="inputState" class="form-control"&gt;
        &lt;option selected&gt;Choose...&lt;/option&gt;
        &lt;option&gt;...&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-2"&gt;
    &lt;label for="inputZip"&gt;Zip&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputZip"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group"&gt;
&lt;div class="form-check pl-0"&gt;
    &lt;div class="custom-control custom-checkbox checkbox-info"&gt;
        &lt;input type="checkbox" class="custom-control-input" id="gridCheck"&gt;
        &lt;label class="custom-control-label" for="gridCheck"&gt;Check me out&lt;/label&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;button type="submit" class="btn btn-primary mt-3"&gt;Sign in&lt;/button&gt;
&lt;/form&gt;
</pre>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    @endsection