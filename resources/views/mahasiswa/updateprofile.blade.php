@extends('layouts.app_ekdom')

@section('judul','Update Profile')


@section('content')

<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-center layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>{{ __('Update Profile') }}</h3>
                                    </div>
                                </div>
                            </div>

                            @include('headerlogin.headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                <div class="widget-content widget-content-area br-6">
                                    <div class="row">
                                        <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                                            <div class="statbox widget box box-shadow">
                                                <div class="widget-header">
                                                    <div class="row">
                                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                            <h4>{{ __('Update Profile Anda') }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="widget-content widget-content-area">
                                                    <form action="{{ url('mahasiswa') }}{{ ('/') }}{{ ('profile') }}{{ ('/') }}{{ $edit->id }}{{ ('/') }}{{ ('mahasiswa') }}" method="POST" enctype="multipart/form-data">
                                                        @method('put')
                                                        @csrf
                                                        <div class="form-row mb-4">
                                                            <div class="form-group col-md-6">
                                                                <label for="inputEmail4">Email</label>
                                                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="inputEmail4" placeholder="Email" value="{{ $edit->email }}">
                                                                @error('email')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="inputPassword4">Nama Lengkap</label>
                                                                <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" id="inputPassword4" placeholder="Nama Lengkap" value="{{ $edit->name }}">
                                                                @error('nama')
                                                                <div class="alert alert-danger">{{ $message }}</div>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">Hobi</label>
                                                            <input type="text" name="hobi" class="form-control @error('hobi') is-invalid @enderror" id="inputAddress2" placeholder="isi hobi anda" value="{{ $edit->hobi }}">
                                                            @error('hobi')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">Alamat</label>
                                                            <input type="text" name="alamat" class="form-control @error('alamat') is-invalid @enderror" id="inputAddress2" placeholder="isi alamat anda" value="{{ $edit->alamat }}">
                                                            @error('alamat')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">Nim</label>
                                                            <input type="text" name="nim" class="form-control @error('nim') is-invalid @enderror" id="inputAddress2" placeholder="Isi Nim" value="{{ $edit->Nim_Nidn_Nip }}">
                                                            @error('nim')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">Jurusan</label>
                                                            <input type="text" name="jurusan" class="form-control @error('jurusan') is-invalid @enderror" id="inputAddress2" placeholder="Isi Sesuai Dengan Jurusanmu" value="{{ $edit->jurusan }}">
                                                            @error('jurusan')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">No Wa </label>
                                                            <input type="number" name="no_wa" class="form-control @error('no_wa') is-invalid @enderror" id="inputAddress2" placeholder="isi no hp anda" value="{{ $edit->no_wa }}">
                                                            @error('no_wa')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <div class="form-group mb-4">
                                                            <label for="inputAddress2">Tentang Diri Anda </label>
                                                            <textarea name="tentang_data_diri" class="form-control @error('tentang_data_diri') is-invalid @enderror" id="inputAddress2" placeholder="isi tentang diri anda">{{ $edit->about }}</textarea>
                                                            @error('tentang_data_diri')
                                                            <div class="alert alert-danger">{{ $message }}</div>
                                                            @enderror
                                                        </div>
                                                        <button type="submit" class="btn btn-primary mt-3">Update Profile</button>
                                                        <a href="{{ url('/mahasiswa/profile/view') }}" class="btn btn-success mt-3">Kembali</a>
                                                    </form>

                                                    <div class="code-section-container">


                                                        <div class="code-section text-left">
                                                            <pre>
&lt;form&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputEmail4"&gt;Email&lt;/label&gt;
    &lt;input type="email" class="form-control" id="inputEmail4" placeholder="Email"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputPassword4"&gt;Password&lt;/label&gt;
    &lt;input type="password" class="form-control" id="inputPassword4" placeholder="Password"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress"&gt;Address&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St"&gt;
&lt;/div&gt;
&lt;div class="form-group mb-4"&gt;
&lt;label for="inputAddress2"&gt;Address 2&lt;/label&gt;
&lt;input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor"&gt;
&lt;/div&gt;
&lt;div class="form-row mb-4"&gt;
&lt;div class="form-group col-md-6"&gt;
    &lt;label for="inputCity"&gt;City&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputCity"&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-4"&gt;
    &lt;label for="inputState"&gt;State&lt;/label&gt;
    &lt;select id="inputState" class="form-control"&gt;
        &lt;option selected&gt;Choose...&lt;/option&gt;
        &lt;option&gt;...&lt;/option&gt;
    &lt;/select&gt;
&lt;/div&gt;
&lt;div class="form-group col-md-2"&gt;
    &lt;label for="inputZip"&gt;Zip&lt;/label&gt;
    &lt;input type="text" class="form-control" id="inputZip"&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;div class="form-group"&gt;
&lt;div class="form-check pl-0"&gt;
    &lt;div class="custom-control custom-checkbox checkbox-info"&gt;
        &lt;input type="checkbox" class="custom-control-input" id="gridCheck"&gt;
        &lt;label class="custom-control-label" for="gridCheck"&gt;Check me out&lt;/label&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;/div&gt;
&lt;button type="submit" class="btn btn-primary mt-3"&gt;Sign in&lt;/button&gt;
&lt;/form&gt;
</pre>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    @endsection
