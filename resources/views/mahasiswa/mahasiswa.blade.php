@extends('layouts.app_ekdom')

@section('judul','Halaman Dashboard')

@section('content')
<!--  BEGIN CONTENT AREA  -->
<div id="content" class="main-content">
    <div class="layout-px-spacing">

        <div class="content-container">

            <div class="col-left layout-top-spacing">
                <div class="col-left-content">

                    <div class="header-container">
                        <header class="header navbar navbar-expand-sm">
                            <div class="d-flex">
                                <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                    <div class="bt-menu-trigger">
                                        <span></span>
                                    </div>
                                </a>
                                <div class="page-header">
                                    <div class="page-title">
                                        <h3>Welcome {{ Auth::user()->role }}</h3>
                                    </div>
                                </div>
                            </div>
                            @include('headerlogin.headerlogin')
                        </header>
                    </div>

                    <div class="admin-data-content layout-top-spacing">

                        <div class="row layout-top-spacing">
                            <div id="btnGroupHorizontal" class="col-lg-12 layout-spacing">
                                <div class="statbox widget box box-shadow">
                                    <div class="widget-header">
                                        <div class="row">
                                            <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                                <h4></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content widget-content-area">
                                        <div _ngcontent-flq-c219="" class="row gx-4 align-items-center">
                                            <div _ngcontent-flq-c219="" class="col-xl-8 col-xxl-12">
                                                <div _ngcontent-flq-c219=""
                                                     class="text-center text-xl-start text-xxl-center px-4 mb-4 mb-xl-0 mb-xxl-4">
                                                    <h1 _ngcontent-flq-c219="" class="text-primary">SELAMAT
                                                        DATANG EKDOM (EVALUASI KINERJA DOSEN OLEH MAHASISWA)</h1>
                                                    <p _ngcontent-flq-c219="" class="text-gray-700 mb-0">Tujuan
                                                        dari evaluasi ini adalah
                                                        untuk meningkatkan kualitas belajar mengajar pada
                                                        institusi kita. Saran dan masukan
                                                        dari mahasiswa merupakan input yang sangat berharga.</p>
                                                </div>
                                            </div>
                                            <div _ngcontent-flq-c219="" class="col-xl-4 col-xxl-12 text-center">
                                                <img _ngcontent-flq-c219="" src="{{ asset('assets/img/uho.png') }}" width="200" height="200"
                                                     class="img-fluid" style="max-width: 26rem;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
