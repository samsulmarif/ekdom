@extends('layouts.app_ekdom')

@section('judul','Hasil Evaluasi')

@section('content')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="content-container">

                <div class="col-center layout-top-spacing">
                    <div class="col-left-content">

                        <div class="header-container">
                            <header class="header navbar navbar-expand-sm">
                                <div class="d-flex">
                                    <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom">
                                        <div class="bt-menu-trigger">
                                            <span></span>
                                        </div>
                                    </a>
                                    <div class="page-header">
                                        <div class="page-title">
                                            <h3>{{ __('Hasil Evaluasi') }}</h3>
                                        </div>
                                    </div>
                                </div>
                                <!-- flash data config nya belakang -->
                            <!-- @if (session('sukses'))
                                <div class="flash-data" data-flashData="{{ session('sukses') }}">
                </div>
            @endif -->
                                @if (session('sukses'))
                                    <div class="alert alert-success">
                                        {{ session('sukses') }}
                                    </div>
                                @endif
                            <!-- buat header login -->
                                @include('headerlogin/headerlogin')
                            </header>
                        </div>

                        <div class="admin-data-content layout-top-spacing">

                            <div class="row layout-top-spacing">
                                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                    <div class="widget-content widget-content-area br-6">
                                        <table id="alter_pagination" class="table table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>email</th>
                                                <th>Role</th>
                                                <th>status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($hasilevaluasi as $h)
                                                @if(Auth::user()->noreg == $h->noreg)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $h->name }}</td>
                                                        <td>{{ $h->email }}</td>
                                                        <td>{{ $h->role }}</td>
                                                        @if(Auth::user()->evaluasi != 'Y')
                                                        <td><span class="badge badge-danger"> Belum Selesai </span></td>
                                                        @else
                                                            <td><span class="badge badge-success"> Sudah Selesai </span></td>
                                                        @endif
                                                    </tr>
                                                    @endif
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>email</th>
                                                <th>Role</th>
                                                <th>status</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
@endsection
