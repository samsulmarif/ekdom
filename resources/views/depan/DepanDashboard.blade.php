@extends('layouts.app_ekdom_depan')

@section('judul','EKDOM FIB UHO')

@section('konten')
<!-- ===============================================-->
<!--    Main Content-->
<!-- ===============================================-->
<main class="main" id="top">


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="bg-primary py-2 d-none d-sm-block">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-auto d-none d-lg-block">
                    <p class="my-2 fs--1"><i class="fas fa-map-marker-alt me-3"></i><span>Jl. H.E.A. Mokodompit, Kampus Hijau Bumi Tridharma Anduonohu Kendari </span></p>
                </div>
                <div class="col-auto">
                    <p class="my-2 fs--1"><i class="fas fa-envelope me-3"></i><a class="text-900" href="mailto:fib@uho.ac.id">fib@uho.ac.id </a></p>
                </div>
            </div>
        </div>
        <!-- end of .container-->

    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


    <nav class="navbar navbar-expand-lg navbar-light sticky-top py-3 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container"><a class="navbar-brand" href="{{ url('/') }}">
                <p>FAKULTAS ILMU BUDAYA UHO</p>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"> </span></button>
            <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto pt-2 pt-lg-0 font-base">
                    @if(Route::has('login'))
                    @auth
                    <li class="nav-item px-2">
                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                            <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                            <circle cx="12" cy="7" r="4"></circle>
                        </svg>
                        <a class="btn btn-primary order-1 order-lg-0" aria-current="page" href="{{ url('/admin/dashboard') }}">{{ Auth::user()->name }}</a>
                    </li>
                    @else
                    <a class="btn btn-primary order-1 order-lg-0" href="{{ url('login') }}">Login</a>
                    @endauth
                    @endif

            </div>
        </div>
    </nav>
    <section class="pt-6 bg-600" id="home">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-5 col-lg-6 order-0 order-md-1 text-end"><img class="pt-7 pt-md-0 w-100" src="{{ asset('assets/img/Logo-UHO.png') }}" width="470" alt="hero-header" /></div>
                <div class="col-md-7 col-lg-6 text-md-start text-center py-6">
                    <h4 class="fw-bold font-sans-serif">Welcome To</h4>
                    <h1 class="fs-6 fs-xl-7 mb-5"> EKDOM (Evaluasi Kinerja Dosen Oleh Mahasiswa) </h1>
                </div>
            </div>
        </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="py-0" style="margin-top:-5.8rem">

        <!-- 1 -->
        <section>

            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb-4">
                        <div class="card h-100"><img class="card-img-top w-100" src="{{ asset('assets/img/Editing body text-bro.png') }}" alt="courses" />
                            <div class="card-body px-xl-5 px-md-3 pt-0">
                                <div class="d-flex" style="margin-top:-3.5rem;">
                                    <img src="{{ asset('assets/img/uho.png') }}" width="100px">
                                </div>
                            </div>
                            <div class="card-body">
                                <h1 class="text-black fs-lg-4 fs-xl-5">Submit Evaluasi!</h1>
                                <h4 class="text-black"><span class="text-primary">Total : @if($data['evaluasi'] > 0) {{ $data['evaluasi'] }} @else {{ __('data kosong') }}@endif</span></h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-4">
                        <div class="card h-100"><img class="card-img-top w-100" src="{{ asset('assets/img/Two factor authentication-pana.png') }}" alt="courses" />
                            <div class="card-body px-xl-5 px-md-3 pt-0">
                                <div class="d-flex" style="margin-top:-3.5rem;">
                                    <img src="{{ asset('assets/img/uho.png') }}" width="100px">
                                </div>
                            </div>
                            <div class="card-body">
                                <h1 class="text-black fs-lg-4 fs-xl-5">Mahasiswa Registered!</h1>
                                <h4 class="text-black"><span class="text-primary">Total : @if($data['mahasiswa'] > 0 ) {{ $data['mahasiswa']  }}@else {{ __('data kosong') }}@endif</span></h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 mb-4">
                        <div class="card h-100"><img class="card-img-top w-100" src="{{ asset('assets/img/Two factor authentication-rafiki.png') }}" alt="courses" />
                            <div class="card-body px-xl-5 px-md-3 pt-0">
                                <div class="d-flex" style="margin-top:-3.5rem;">
                                    <img src="{{ asset('assets/img/uho.png') }}" width="100px">
                                </div>
                            </div>
                            <div class="card-body">
                                <h1 class="text-black fs-lg-4 fs-xl-5">Dosen Registered!</h1>
                                <h4 class="text-black"><span class="text-primary">Total : @if($data['dosen'] > 0 ) {{ $data['dosen']}} @else {{ __('data kosong') }}@endif</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </section>
    <br>
    <!-- <section> begin ============================-->
    <section class="bg-secondary">

        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-6 mb-4 order-0 order-sm-0"><a class="text-decoration-none" href="#"><img src="{{ asset('assets/img/Logo-UHO.png') }}" height="150" alt="" /></a>
                    <p class="text-light my-4"> <i class="fas fa-map-marker-alt me-3"></i><span class="text-light"></span><a class="text-light" href="tel:+604-680-9785">Jl. H.E.A. Mokodompit, </a><br />Kampus Hijau Bumi Tridharma Anduonohu Kendari</p>
                    <p class="text-light"> <i class="fas fa-envelope me-3"> </i><a class="text-light" href="mailto:fib@uho.ac.id">fib@uho.ac.id</a></p>
                    <p class="text-light"> <i class="fas fa-phone-alt me-3"></i><a class="text-light" href="tel:04013084783, 3195132">04013084783, 3195132 (FIB)</a></p>
                </div>
                <div class="col-6 col-sm-4 col-lg-2 mb-3 order-2 order-sm-1">
                    <h5 class="lh-lg fw-bold mb-4 text-light font-sans-serif">Peta FIB Universitas Halu Oleo </h5>
                    <ul class="list-unstyled mb-md-4 mb-lg-0">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3980.0417608019275!2d122.5208393884516!3d-4.011832727411389!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2d988d6c29e36b1d%3A0x6e35317a5d43423f!2sFakultas%20Ilmu%20Budaya%20-%20UHO!5e0!3m2!1sid!2sid!4v1631419444926!5m2!1sid!2sid" width="400" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </ul>
                </div>
            </div>
        </div>
        <!-- end of .container-->

    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


    <section class="py-0" style="margin-top: -5.8rem;">
        <div class="container bg-primary">
            <div class="row justify-content-md-between justify-content-evenly py-4">
                <div class="col-12 col-sm-8 col-md-6 col-lg-auto text-center text-md-start">
                    <p class="fs--1 my-2 fw-bold">All rights Reserved &copy; Your FIB, {{ now()->year }} Developed By Greentech Studio</p>
                </div>
                <div class="col-12 col-sm-8 col-md-6">
                </div>
            </div>
        </div>
    </section>
</main>
<!-- ===============================================-->
<!--    End of Main Content-->
<!-- ===============================================-->


@endsection