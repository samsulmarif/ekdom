<div class="footer-wrapper col-xl-12">
    <div class="footer-section f-section-1">
        <p class="">@copyright {{ now()->year }}<a target="_blank" href="https://designreset.com">EKDOM</a></p>
    </div>
    <div class="footer-section f-section-2">
        <p class="">Developed By <a href="http://greentech.id">GreentechStudio</a></p>
    </div>
</div>
</div>
</div>

</div>
</div>

</div>
<!--  END CONTENT AREA  -->
</div>
<!-- END MAIN CONTAINER -->