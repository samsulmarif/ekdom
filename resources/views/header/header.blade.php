<!-- BEGIN LOADER -->

<body class="dashboard-analytics admin-header">
    <div id="load_screen">
        <div class="loader">
            <div class="loader-content">
                <div class="spinner-grow align-self-center"></div>
            </div>
        </div>
    </div>
    <!--  END LOADER -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        <div class="sidebar-wrapper sidebar-theme">

            <div class="theme-logo">
                <a href="{{ url('/dashboard') }}">
                    <img src="{{ asset('uho.png') }}" alt="logo" width="60" height="40">
                    <span class="admin-logo">EKDOM<span></span></span>
                </a>
            </div>

            <div class="sidebarCollapseFixed">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left">
                    <line x1="19" y1="12" x2="5" y2="12"></line>
                    <polyline points="12 19 5 12 12 5"></polyline>
                </svg>
            </div>
            @if(Auth::user()->role == 'administrator')
            <nav id="compactSidebar">
                <ul class="menu-categories">
                    <li class="menu menu-single">
                        <a href="{{ url('admin/dashboard') }}" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                    </svg>
                                </div>
                                <span>{{ __('Dashboard')}}</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="#prasarana" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <div class="base-icons">
                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <span>{{ __('Evaluasi') }}</span>
                                <br>
                                <span>{{ __('Prasarana') }}</span>

                            </div>
                        </a>
                    </li>
                    <li class="menu">
                        <a href="#staf" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <div class="base-icons">
                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <span>{{ __('Evaluasi') }}</span>
                                <br>
                                <span>{{ __('Staf') }}</span>

                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="#evaluasi" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <div class="base-icons">
                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <span>{{ __('Evaluasi') }}</span>
                                <br>
                                <span>{{ __('Kinerja Dosen') }}</span>

                            </div>
                        </a>
                    </li>

                    <li class="menu menu-single">
                        <a href="{{ url('admin/hasil_evaluasi_prasarana') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <polyline points="9 11 12 14 22 4"></polyline>
                                        <path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path>
                                    </svg>
                                </div>
                                <span>{{ __('Hasil Evaluasi') }}</span>
                                <br>
                                <span>{{ __('Prasarana') }}</span>

                            </div>
                        </a>
                    </li>
                    <li class="menu menu-single">
                        <a href="{{ url('admin/hasil_evaluasi_staf') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <polyline points="9 11 12 14 22 4"></polyline>
                                        <path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path>
                                    </svg>
                                </div>
                                <span>{{ __('Hasil Evaluasi') }}</span>
                                <br>
                                <span>{{ __('Staf') }}</span>

                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="#tables" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <line x1="22" y1="12" x2="2" y2="12"></line>
                                        <path d="M5.45 5.11L2 12v6a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2v-6l-3.45-6.89A2 2 0 0 0 16.76 4H7.24a2 2 0 0 0-1.79 1.11z"></path>
                                        <line x1="6" y1="16" x2="6.01" y2="16"></line>
                                        <line x1="10" y1="16" x2="10.01" y2="16"></line>
                                    </svg>
                                </div>
                                <span>Import Data</span>
                            </div>
                        </a>
                    </li>
                    <li class="menu menu-single">
                        <a href="{{ url('/admin/dataApi') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="18" cy="18" r="3"></circle><circle cx="6" cy="6" r="3"></circle><path d="M6 21V9a9 9 0 0 0 9 9"></path></svg>
                                </div>
                                <span>Data Api</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="#pages" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="9" cy="7" r="4"></circle>
                                        <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
                                        <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
                                    </svg>
                                </div>
                                <span>{{ __('Users') }}</span>
                                <br>
                                <span>{{ __('Management') }}</span>
                            </div>
                        </a>
                    </li>

                </ul>
            </nav>
            @elseif(Auth::user()->role == 'mahasiswa')
            <nav id="compactSidebar">
                <ul class="menu-categories">
                    <li class="menu menu-single">
                        <a href="{{ url('mahasiswa/dashboard') }}" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                    </svg>
                                </div>
                                <span>{{ __('Dashboard')}}</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu menu-single">
                        <a href="{{ url('mahasiswa/profile/view') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg>
                                </div>
                                <span>{{ __('Profile') }}</span>
                            </div>
                        </a>
                    </li>
                    <li class="menu menu-single">
                        <a href="{{ url('mahasiswa/reset_password') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                        <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                    </svg>
                                </div>
                                <span>{{ __('Ubah Password') }}</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu menu-single">
                        <a href="{{ url('mahasiswa/evaluasi-saya-dosen') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>
                                        <polyline points="14 2 14 8 20 8"></polyline>
                                        <line x1="16" y1="13" x2="8" y2="13"></line>
                                        <line x1="16" y1="17" x2="8" y2="17"></line>
                                        <polyline points="10 9 9 9 8 9"></polyline>
                                    </svg>
                                </div>
                                <span>{{ __('Evaluasi Saya') }}</span>
                                <br>
                                <span>{{ __('Kinerja Dosen') }}</span>
                            </div>
                        </a>
                    </li>
                    <li class="menu menu-single">
                        <a href="{{ url('mahasiswa/evaluasi-saya-prasarana') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <line x1="16.5" y1="9.4" x2="7.5" y2="4.21"></line>
                                        <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                        <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                        <line x1="12" y1="22.08" x2="12" y2="12"></line>
                                    </svg>
                                </div>
                                <span>{{ __('Evaluasi Saya') }}</span>
                                <br>
                                <span>{{ __('Prasarana') }}</span>
                            </div>
                        </a>
                    </li>
                    <li class="menu menu-single">
                        <a href="{{ url('mahasiswa/evaluasi') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <line x1="22" y1="2" x2="11" y2="13"></line>
                                        <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                    </svg>
                                </div>
                                <span>{{ __('Kirim Evaluasi') }}</span>
                            </div>
                        </a>
                    </li>

                </ul>
            </nav>
            @elseif(Auth::user()->role == 'dosen')
            <nav id="compactSidebar">
                <ul class="menu-categories">
                    <li class="menu menu-single">
                        <a href="{{ url('dosen/dashboard') }}" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                                        <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                        <polyline points="9 22 9 12 15 12 15 22"></polyline>
                                    </svg>
                                </div>
                                <span>{{ __('Dashboard')}}</span>
                            </div>
                        </a>
                    </li>

                    <!-- BEGIN:: NEXT PROJECT PENGEMBANGAN EKDOM (DOSEN BISA MEMBUAT SENDIRI PERTANYAAN EVALUASINYA ) -->
                    <!-- <li class="menu">
                        <a href="#pertanyaan" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <div class="base-icons">
                                        <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                            <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                        </svg>
                                    </div>
                                </div>
                                <span>{{ __('Pertanyaan') }}</span>
                                <br>
                                <span>{{ __('Evaluasi') }}</span>

                            </div>
                        </a>
                    </li> -->
                    <!-- END:: NEXT PROJECT PENGEMBANGAN EKDOM (DOSEN BISA MEMBUAT SENDIRI PERTANYAAN EVALUASINYA ) -->


                    <li class="menu menu-single">
                        <a href="{{ url('dosen/hasil_evaluasi_mahasiswa') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <polyline points="9 11 12 14 22 4"></polyline>
                                        <path d="M21 12v7a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11"></path>
                                    </svg>
                                </div>
                                <span>{{ __('Evaluasi Oleh') }}</span>
                                <br>
                                <span>{{ __('Mahasiswa') }}</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu menu-single">
                        <a href="{{ url('dosen/profile/view') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>
                                        <circle cx="12" cy="7" r="4"></circle>
                                    </svg>
                                </div>
                                <span>{{ __('Profile') }}</span>
                            </div>
                        </a>
                    </li>
                    <li class="menu menu-single">
                        <a href="{{ url('dosen/evaluasi') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <line x1="22" y1="2" x2="11" y2="13"></line>
                                        <polygon points="22 2 15 22 11 13 2 9 22 2"></polygon>
                                    </svg>
                                </div>
                                <span>{{ __('Kirim Evaluasi') }}</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu menu-single">
                        <a href="{{ url('dosen/reset_password') }}" data-active="false" class="menu-toggle">
                            <div class="base-menu">
                                <div class="base-icons">
                                    <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1">
                                        <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
                                        <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                                    </svg>
                                </div>
                                <span>{{ __('Ubah Password') }}</span>
                            </div>
                        </a>
                    </li>

                </ul>
            </nav>
            @endif

            <div id="compact_submenuSidebar" class="submenu-sidebar">

                <div class="submenu" id="components">
                    <ul class="submenu-list" data-parent-element="#components">
                        <li>
                            <a href="component_tabs.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Tabs </a>
                        </li>
                        <li>
                            <a href="component_accordion.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Accordions </a>
                        </li>
                        <li>
                            <a href="component_modal.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Modals </a>
                        </li>
                        <li>
                            <a href="component_cards.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Cards </a>
                        </li>
                        <li>
                            <a href="component_bootstrap_carousel.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Carousel</a>
                        </li>
                        <li>
                            <a href="component_blockui.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Block UI </a>
                        </li>
                        <li>
                            <a href="component_countdown.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Countdown </a>
                        </li>
                        <li>
                            <a href="component_counter.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Counter </a>
                        </li>
                        <li>
                            <a href="component_sweetalert.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Sweet Alerts </a>
                        </li>
                        <li>
                            <a href="component_timeline.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Timeline </a>
                        </li>
                        <li>
                            <a href="component_snackbar.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Notifications </a>
                        </li>
                        <li>
                            <a href="component_session_timeout.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Session Timeout </a>
                        </li>
                        <li>
                            <a href="component_media_object.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Media Object </a>
                        </li>
                        <li>
                            <a href="component_list_group.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> List Group </a>
                        </li>
                        <li>
                            <a href="component_pricing_table.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Pricing Tables </a>
                        </li>
                        <li>
                            <a href="component_lightbox.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Lightbox </a>
                        </li>
                    </ul>
                </div>

                <div class="submenu" id="forms">
                    <ul class="submenu-list" data-parent-element="#forms">
                        <li>
                            <a href="form_bootstrap_basic.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Basic </a>
                        </li>
                        <li>
                            <a href="form_input_group_basic.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Input Group </a>
                        </li>
                        <li>
                            <a href="form_layouts.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Layouts </a>
                        </li>
                        <li>
                            <a href="form_validation.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Validation </a>
                        </li>
                        <li>
                            <a href="form_input_mask.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Input Mask </a>
                        </li>
                        <li>
                            <a href="form_bootstrap_select.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Bootstrap Select </a>
                        </li>
                        <li>
                            <a href="form_select2.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Select2 </a>
                        </li>
                        <li>
                            <a href="form_bootstrap_touchspin.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> TouchSpin </a>
                        </li>
                        <li>
                            <a href="form_maxlength.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Maxlength </a>
                        </li>
                        <li>
                            <a href="form_checkbox_radio.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Checkbox &amp; Radio </a>
                        </li>
                        <li>
                            <a href="form_switches.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Switches </a>
                        </li>
                        <li>
                            <a href="form_wizard.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Wizards </a>
                        </li>
                        <li>
                            <a href="form_fileupload.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> File Upload </a>
                        </li>
                        <li>
                            <a href="form_quill.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Quill Editor </a>
                        </li>
                        <li>
                            <a href="form_markdown.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Markdown Editor </a>
                        </li>
                        <li>
                            <a href="form_date_range_picker.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Date &amp; Range Picker </a>
                        </li>
                        <li>
                            <a href="form_clipboard.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Clipboard </a>
                        </li>
                        <li>
                            <a href="form_typeahead.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Typeahead </a>
                        </li>
                    </ul>
                </div>
                <!-- buat menu pertanyaan dosen-->
                <div class="submenu" id="pertanyaan">
                    <ul class="submenu-list" data-parent-element="#pertanyaan">
                        <li>
                            <a href="{{ url('/dosen/pertanyaan_evaluasi') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Pertanyaan Evaluasi </a>
                        </li>
                        <li>
                            <a href="{{ url('/dosen/evaluasi/index') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Evaluasi </a>
                        </li>
                        <li>
                            <a href="{{ url('/dosen/evaluasi') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Tambah Evaluasi </a>
                        </li>
                        <li>
                            <a href="{{ url('/dosen/tambah_pertanyaan') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Tambah Pertanyaan </a>
                        </li>
                    </ul>
                </div>
                <!-- buat menu evaluasi dosen-->
                <div class="submenu" id="evaluasi">
                    <ul class="submenu-list" data-parent-element="#evaluasi">
                        <li>
                            <a href="{{ url('/admin/evaluasi') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Evaluasi </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/pertanyaan_evaluasi') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Pertanyaan</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/tambah_pertanyaan') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Tambah Pertanyaan </a>
                        </li>
                    </ul>
                </div>
                <!-- buat menu prasarana fakultas-->
                <div class="submenu" id="prasarana">
                    <ul class="submenu-list" data-parent-element="#prasarana">
                        <li>
                            <a href="{{ url('/admin/prasarana') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Evaluasi </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/pertanyaan_prasarana') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Pertanyaan</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/tambah_pertanyaan_prasarana') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Tambah Pertanyaan </a>
                        </li>
                    </ul>
                </div>

                <!-- buat menu staf fakultas-->
                <div class="submenu" id="staf">
                    <ul class="submenu-list" data-parent-element="#staf">
                        <li>
                            <a href="{{ url('/admin/staf') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Evaluasi </a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/pertanyaan_staf') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Pertanyaan</a>
                        </li>
                        <li>
                            <a href="{{ url('/admin/tambah_pertanyaan_staf') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Tambah Pertanyaan </a>
                        </li>
                    </ul>
                </div>

                <div class="submenu" id="tables">
                    <ul class="submenu-list" data-parent-element="#tables">
                        <li>
                            <a href="table_basic.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Import Data Mahasiswa</a>
                        </li>
                        <li>
                            <a href="table_basic.html"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> Import Data Dosen</a>
                        </li>
                    </ul>
                </div>

                <div class="submenu" id="pages">
                    <ul class="submenu-list" data-parent-element="#pages">
                        <li>
                            <a href="{{ url('admin/users') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> {{ __('List Akun Admin') }} </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/mahasiswa') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> {{ __('List Akun Mahasiswa') }} </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/dosen') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> {{ __('List Akun Dosen') }} </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/users-forms') }}"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-git-commit">
                                        <circle cx="12" cy="12" r="4"></circle>
                                        <line x1="1.05" y1="12" x2="7" y2="12"></line>
                                        <line x1="17.01" y1="12" x2="22.96" y2="12"></line>
                                    </svg></span> {{ __('Tambah Users') }} </a>
                        </li>

                    </ul>
                </div>


            </div>

        </div>
        <!--  END SIDEBAR  -->
